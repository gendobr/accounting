<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Kurs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kurs-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-4">
            <?php /* $form->field($model, 'date_kurs')->textInput() */ ?>
            <?=
            $form->field($model, 'date_kurs')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Дата'],
                'convertFormat'=>true,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-M-dd'
                ]
            ])
            ?>

        </div>
        <div class="col-sm-4"><?= $form->field($model, 'value_kurs')->textInput() ?></div>
        <div class="col-sm-4">
            <label class="control-label" for="kurs-submit">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>






<?php ActiveForm::end(); ?>

</div>
