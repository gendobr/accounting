<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KursSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курси UAH/USD';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a('Додати курс', ['create'], ['class' => 'btn btn-xs btn-success']) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                //    'view' => function ($url, $model, $key) {
                //        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['product/view', 'id' => $model['product_id']], ['title' => Yii::t('app', 'View')]);
                //    }
                ]
            ],            // ['class' => 'yii\grid\SerialColumn'],
            // 'id_kurs',
            'date_kurs',
            'value_kurs',

        ],
    ]);
    ?>
</div>
