<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */

$this->title = "Курс UAH/USD на ".$model->date_kurs;
$this->params['breadcrumbs'][] = ['label' => 'Курси UAH/USD', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Змінити', ['update', 'id' => $model->id_kurs], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_kurs], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kurs',
            'date_kurs',
            'value_kurs',
        ],
    ]) ?>

</div>
