<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$company=$model->idCompany;

$this->title = $model->name_unit;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Одиниці вимірювання', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-view">

    <h1><?= Html::encode($this->title) ?>

    
        <?= Html::a('Змінити', ['update', 'id' => $model->id_unit], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_unit], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_unit',
            // 'id_company',
            'name_unit',
        ],
    ]) ?>

</div>
