<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */
$company=$model->idCompany;

$this->title = 'Змінити одиницю вимірювання: ' . $model->name_unit;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Одиниці вимірювання', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => $model->name_unit, 'url' => ['view', 'id' => $model->id_unit]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
