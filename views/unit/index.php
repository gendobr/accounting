<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Одиниці вимірювання';

$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Створити одиницю вимірювання', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            
            ['attribute' => 'id_unit', 'filterOptions' => ['class' => 'column-id'],],
            // 'id_company',
            'name_unit',

            
        ],
    ]); ?>
</div>
