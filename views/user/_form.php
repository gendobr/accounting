<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lastname_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login_user')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'email_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note_user')->textInput(['maxlength' => true]) ?>
    
    
    <?php
    echo $form->field($model, 'id_role')->widget(Select2::classname(), [
        'data' => User::listAll(),
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Оберіть роль ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    
    ?>
    
    <div class="form-group field-user-note_user">
        
        <h2>Пароль</h2>
        <label class="control-label" for="user-note_user">(друкуйте тільки якщо ви бажаєте змінити пароль)</label>
        <?php
        echo Html::input('password', 'upw1', '', ['class' => 'form-control']);
        ?>
        <label class="control-label" for="user-note_user">Надрукуйте пароль ще раз</label>
        <?php
        echo Html::input('password', 'upw2', '', ['class' => 'form-control']);
        ?>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
