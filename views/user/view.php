<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "{$model->login_user} ({$model->id_user})";
$this->params['breadcrumbs'][] = ['label' => 'Користувачі', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Змінити', ['update', 'id' => $model->id_user], ['class' => 'btn btn-xs btn-primary']) ?>
        <?=
        Html::a('Видалити', ['delete', 'id' => $model->id_user], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ])
        ?>
    </h1>
    <?php
    $roles=$model->getRoles();
    $roleview=[];
    foreach($roles as $role){
        if($role->idCompany){
            $roleview[]="{$role->name_role} {$role->idCompany->name_company}";
        }else{
            $roleview[]="{$role->name_role}";
        }
    }
    $roleview = join(', ',$roleview);
    
    ?>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user',
            'lastname_user',
            'firstname_user',
            'surname_user',
            'login_user',
            // 'password_user',
            'email_user:email',
            [
                'attribute' => 'id_role',
                'label' =>'Ролі користувача',
                'value' => $roleview,
                // 'visible' => \Yii::$app->user->can('posts.owner.view'),
            ],
            'note_user',
        ],
    ])
    ?>

</div>
