<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Користувачі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a('Створити користувача', ['create'], ['class' => 'btn btn-xs btn-success']) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            ['attribute' => 'id_user', 'filterOptions' => ['class' => 'column-id'],],
            'lastname_user',
            'firstname_user',
            'surname_user',
            'login_user',
            [
                'label'=>'Ролі',
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'value' => function ($data) {
                    $roles = $data->getRoles();
                    $roleview = [];
                    foreach ($roles as $role) {
                        if ($role->idCompany) {
                            $roleview[] = "{$role->name_role} {$role->idCompany->name_company}";
                        } else {
                            $roleview[] = "{$role->name_role}";
                        }
                    }
                    $roleview = join(', ', $roleview);
                    return $roleview; // $data['name'] for array data, e.g. using SqlDataProvider.
                },
                    ],
                    // 'password_user',
                    // 'email_user:email',
                    // 'note_user',
                    // 'role_user',
                    // 'id_company',
                    
                ],
            ]);
            ?>
</div>
