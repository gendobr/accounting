<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['siteTitle'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    
    $topMenuItems=[];
    
    $dateSaleKurs = app\models\Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();
    if($dateSaleKurs){
        $topMenuItems['kurs']='<li><a style="color:white;cursor:default;">Курс '.$dateSaleKurs->value_kurs.' ГРН/USD </a></li>';
    }else{
        $topMenuItems['kurs']='<li><a href="index.php?r=kurs%2Fcreate" style="color:white;">Курс ГРН/USD не задано</a></li>';
    }
    
    // $topMenuItems['000']=['label' => 'Home', 'url' => ['/site/index']];
    // $topMenuItems[]=['label' => 'About', 'url' => ['/site/about']];
    // $topMenuItems[]=['label' => 'Contact', 'url' => ['/site/contact']];
    
    // today currency exchange course
    
    if(Yii::$app->user->isGuest){
        $topMenuItems['100']=['label' => 'Login', 'url' => ['/site/login']];
    }else{

        if (Yii::$app->user->identity->is("admin|booker")){
            $topMenuItems['060']=['label' => 'Керування', 'items'=>[] ];
            
            $topMenuItems['060']['items'][]=['label' => 'Звіти', 'url' => ['/report/index']];
            if (Yii::$app->user->identity->is("admin")){
                $topMenuItems['060']['items'][]=['label' => 'Підприємства', 'url' => ['/company/index']];
                $topMenuItems['060']['items'][]=['label' => 'Користувачі', 'url' => ['/user/index']];
            }
            $topMenuItems['060']['items'][]=['label' => 'Курс UAH/USD', 'url' => ['/kurs/index']];
        }
        
        //
        //        if (Yii::$app->user->identity->is("admin|booker|operator=*")){
        //            // $topMenuItems['080']=['label' => 'Облік', 'items'=>[] ];
        //        }

        
        $topMenuItems['100']=(
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->login_user . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
        );
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $topMenuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=Yii::$app->params['siteTitle']?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
