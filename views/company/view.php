<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = $model->name_company;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

        <p>
        <?=Html::a('<span class="glyphicon glyphicon-download-alt"></span> Продажі', 
                                       ['/sale/index', 'id_company' => $model->id_company], 
                                       ['title' => "Продажі",'class' => 'btn btn-xs btn-success'])?>
        <?=Html::a('<span class="glyphicon glyphicon-upload"></span> Покупки', 
                                       ['/buy/index', 'id_company' => $model->id_company], 
                                       ['title' => "Покупки",'class' => 'btn btn-xs btn-success']
                                      )?>
        <?=Html::a('<span class="glyphicon glyphicon-book"></span> Платежі', 
                                      ['/payment/index', 'id_company' => $model->id_company], 
                                      ['title' => "Платежі",'class' => 'btn btn-xs btn-success'])?>

        <?=Html::a('<span class="glyphicon glyphicon-book"></span> Звіти', 
                                      ['/report/index', 'id_company' => $model->id_company], 
                                      ['title' => "Звіти",'class' => 'btn btn-xs btn-success'])?>
        
        <?=Html::a('<span class="glyphicon glyphicon-user"></span> Партнери', 
                                      ['/partner/index', 'id_company' => $model->id_company], 
                                      ['title' => "Партнери",'class' => 'btn btn-xs btn-primary'])?>
        <?=Html::a('<span class="glyphicon glyphicon-piggy-bank"></span> Товари', 
                                ['/product/index', 'id_company' => $model->id_company], 
                                ['title' => "Товари",'class' => 'btn btn-xs btn-primary'])?>
        <?=    Html::a('<span class="glyphicon glyphicon-upload"></span> Місця використання', 
                                       ['/utilized-place/index', 'id_company' => $model->id_company], 
                                       ['title' => "Місця використання",'class' => 'btn btn-xs btn-primary']
                                      ) ?>

        <?=Html::a('<span class="glyphicon glyphicon-compressed"></span> Одиниці вимірювання', 
                                ['/unit/index', 'id_company' => $model->id_company], 
                                ['title' => "Одиниці вимірювання",'class' => 'btn btn-xs btn-primary'])?>
        <?=Html::a('<span class="glyphicon glyphicon-tag"></span> Категорії товарів', 
                                ['/category/index', 'id_company' => $model->id_company], 
                                ['title' => "Категорії товарів",'class' => 'btn btn-xs btn-primary'])?>

            
            
            
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Змінити', ['update', 'id' => $model->id_company], ['class' => 'btn btn-xs btn-primary']) ?>
            
        <?php if(Yii::$app->user->identity->is("admin")) { ?>
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span> Видалити', ['delete', 'id' => $model->id_company], [
                    'class' => 'btn btn-xs btn-danger',
                    'data' => [
                        'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                        'method' => 'post',
                    ],
                ]) ?>
        <?php }?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_company',
            'name_company',
            'kontakt_company',
            'note_company',
        ],
    ]) ?>

</div>
