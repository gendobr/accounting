<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Підприємства';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?>
        <?php if(Yii::$app->user->identity->is("admin")) { ?>
        <?= Html::a('Створити підприємство', ['create'], ['class' => 'btn btn-xs btn-success']) ?>
        <?php } ?>
    </h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '
                    <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="{key}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Дії
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="{key}">
                      <li>{sale}</li>
                      <li>{buy}</li>
                      <li>{payment}</li>
                      <!-- <li>{report}</li> -->
                      <li role="separator" class="divider"></li>
                      <li>{utilizationplaces}</li>
                      <li>{partner}</li>
                      <li>{product}</li>
                      <li>{unit}</li>
                      <li>{category}</li>
                      <li role="separator" class="divider"></li>
                      <li>{view}</li>
                      <li>{update}</li>
                      <li role="separator" class="divider"></li>
                      <li>{delete}</li>
                    </ul>
                  </div>
                 ',
                'buttons' => [
                    'key'=>function($url, $model, $key){
                        return 'dropdownMenu'.$key;
                    },
                    'payment'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span> Платежі', 
                                       ['/payment/index', 'id_company' => $model->id_company], 
                                       ['title' => "Платежі"]);
                    },
                    'sale' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span> Продажі', 
                                       ['/sale/index', 'id_company' => $model->id_company], 
                                       ['title' => "Продажі"]);
                    },
                    'buy' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-upload"></span> Покупки', 
                                       ['/buy/index', 'id_company' => $model->id_company], 
                                       ['title' => "Покупки"]
                                      );
                    },
                    'utilizationplaces' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-upload"></span> Місця використання', 
                                       ['/utilized-place/index', 'id_company' => $model->id_company], 
                                       ['title' => "Місця використання"]
                                      );
                    },
                    'report' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-book"></span> Звіти', 
                                      ['/report/index', 'id_company' => $model->id_company], 
                                      ['title' => "Звіти"]);
                    },        
                    'partner' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span> Партнери', 
                                      ['/partner/index', 'id_company' => $model->id_company], 
                                      ['title' => "Партнери"]);
                    },        
                    'product' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-piggy-bank"></span> Товари', 
                                ['/product/index', 'id_company' => $model->id_company], 
                                ['title' => "Товари"]);
                    },
                    'unit' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-compressed"></span> Одиниці вимірювання', 
                                ['/unit/index', 'id_company' => $model->id_company], 
                                ['title' => "Одиниці вимірювання"]);
                    },
                    'category' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-tag"></span> Категорії товарів', 
                                ['/category/index', 'id_company' => $model->id_company], 
                                ['title' => "Категорії товарів"]);
                    },                            
                    'view'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Переглянути', 
                                ['view', 'id' => $model->id_company], 
                                ['title' => "Переглянути"]);
                    },
                    'update'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span> Змінити', 
                                ['update', 'id' => $model->id_company], 
                                ['title' => "Змінити"]);
                    },
                    'delete'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span> Видалити', 
                                ['delete', 'id' => $model->id_company], 
                                ['title' => "Видалити","data-method"=>"post"]);
                    },
                ]
            ],
            ['attribute' => 'id_company', 'filterOptions' => ['class' => 'column-id'],],
            'name_company',
            'kontakt_company',
            'note_company',


        ],
    ]); ?>
</div>
