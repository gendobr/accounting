<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = 'Змінити підприємство: ' . $model->name_company;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_company, 'url' => ['view', 'id' => $model->id_company]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
