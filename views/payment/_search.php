<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_payment') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'id_partner') ?>

    <?= $form->field($model, 'date_payment') ?>

    <?= $form->field($model, 'value_kurs_payment') ?>

    <?php // echo $form->field($model, 'amount_uah_payment') ?>

    <?php // echo $form->field($model, 'amount_usd_payment') ?>

    <?php // echo $form->field($model, 'type_payment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
