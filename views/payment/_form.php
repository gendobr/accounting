<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Partner;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
  if($error=Yii::$app->session->getFlash('error')){
      ?><div class="alert alert-danger" role="alert"><?=$error?></div><?php
  }
?>


<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_company')->label('')->hiddenInput() ?>

    <?php
    echo $form->field($model, 'id_partner')->widget(Select2::classname(), [
        'data' => Partner::listAll($model->id_company),
        'showToggleAll' => false,
        'options' => ['placeholder' => 'Оберіть партнера ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    
    <div class="row">
        <div class="col-sm-4">
            <label class="control-label" for="payment-date-payment">Дата платежу</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_payment',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'amount_uah_payment')->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'type_payment')->dropDownList([ 'income' => 'Вхідний', 'outcome' => 'Вихідний',], ['prompt' => '']) ?>
        </div>
    </div>

    <?= $form->field($model, 'is_used')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
