<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$company = $model->idCompany;
$this->title = 'Платіж ' . $model->id_payment;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Платежі', 'url' => ['index', 'id_company' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;

$paymentTypeList = $model->getPaymentTypeList();



$percentage='';
switch($model->type_payment){
    case 'income':
        $incomes=$model->getIncomeSales()->all();
        $incomeAmount=0;
        foreach($incomes as $income){
            $incomeAmount+=$income->amount_usd_income_sale;
        }
        $percentage=round(100 * $incomeAmount / $model->amount_usd_payment ,2)."%, {$incomeAmount} USD";
        break;
    case 'outcome':
        $outcome=$model->getSpendBuys()->all();
        $outcomeAmount=0;
        foreach($outcome as $income){
            $outcomeAmount+=$income->amount_usd_spend_buy;
        }
        $percentage=round(100 * $outcomeAmount / $model->amount_usd_payment ,2)."% , {$outcomeAmount} USD";
        break;
}
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Змінити', ['update', 'id' => $model->id_payment], ['class' => 'btn btn-xs btn-primary']) ?>
        
        <?php
        switch($model->type_payment){
            case 'income':
                echo Html::a('Продажі', ['sale', 'id' => $model->id_payment], ['class' => 'btn btn-xs btn-primary']);
                echo " ";
                echo Html::a('Новий продаж', ['/sale/create', 'id_company' => $company->id_company], ['class' => 'btn btn-xs btn-success']);
                break;
            case 'outcome':
                echo Html::a('Покупки', ['buy', 'id' => $model->id_payment], ['class' => 'btn btn-xs btn-primary']);
                echo " ";
                echo Html::a('Нова покупка', ['/buy/create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']);
                break;
        }
        ?>
        <?=
        Html::a('Видалити', ['delete', 'id' => $model->id_payment], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ])
        ?>
    </h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_payment',
            // 'id_company',
            // 'id_partner',
            // 'date_payment',
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'format' => 'html',
                'value' => ( $model->idPartner ? Html::a("{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner]) : ''),
            ],
            [
                'attribute' => 'date_payment',
                'label' => 'Дата платежу',
                'value' => date('d.m.Y', strtotime($model->date_payment)),
            ],
            'value_kurs_payment',
            'amount_uah_payment',
            'amount_usd_payment',
            //'type_payment',
            [
                'attribute' => 'type_payment',
                'label' => 'Тип ',
                'value' => (isset($paymentTypeList[$model->type_payment]) ? $paymentTypeList[$model->type_payment] : $model->type_payment ),
            ],
            [
                'attribute' => 'is_used',
                'label' =>'Використано',
                'value' => $percentage,
            ],
        ],
    ])
    ?>

    <div>
        <?php
        switch($model->type_payment){
            case 'income':
                
                if($incomes){
                    ?>
                    <h4>Оплачені продажі</h4>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th></th>
                            <!-- <th>Партнер</th> -->
                            <th>Товар</th>
                            <th>Кількість товару</th>
                            <th>Дата продажу</th>
                            <th colspan="2">Надходження</th>
                        </tr>
                    <?php
                    foreach($incomes as $income){
                        $sale=$income->idSale;
                        ?>
                        <tr>
                            <td><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/sale/view', 'id' => $sale->id_sale], ['class' => 'btn btn-xs btn-primary']) ?></td>
                            <!-- <td><?=$sale->idPartner->name_partner ?></td> -->
                            <td><?=$sale->name_product?></td>
                            <td><?=$sale->amount.'&nbsp'.$sale->idUnit->name_unit ?></td>
                            <td><?=date('d.m.Y',strtotime($sale->date_sale))?></td>
                            <td><?=round($income->amount_uah_income_sale,2)?> грн</td>
                            <td><?=round($income->amount_usd_income_sale,2)?> usd</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                    <?php                    
                }
                break;
            case 'outcome':
                
                if($outcome){
                    ?>
                    <h4>Оплачені покупки</h4>
                    <table class="table table-striped table-bordered">
                    <?php
                    foreach($outcome as $outcome){
                        $buy=$outcome->idBuy;
                        ?>
                        <tr>

                            <td><?=$buy->name_product?></td>
                            <td><?=$buy->idPartner->name_partner ?></td>
                            <td><?=$buy->amount_buy.'&nbsp'.$buy->idUnit->name_unit ?></td>
                            <td><?=$buy->date_buy?></td>
                            <td><?=$outcome->amount_grn_spend_buy?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                    <?php                    
                }
                break;   
        }
        ?>
    </div>
</div>
