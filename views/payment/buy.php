<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$company = $model->idCompany;
$this->title = 'Оплата покупок';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Платежі', 'url' => ['index', 'id_company' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Платіж: ' . $model->id_payment, 'url' => ['view', 'id' => $model->id_payment]];
$this->params['breadcrumbs'][] = $this->title;

$paymentTypeList = $model->getPaymentTypeList();



$percentage = '';

$outcome = $model->getSpendBuys()->all();
$outcomeAmount = 0;
foreach ($outcome as $income) {
    $outcomeAmount+=$income->amount_usd_spend_buy;
}
$percentage = round(100 * $outcomeAmount / $model->amount_usd_payment, 2) . "% , {$outcomeAmount} USD";
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Партнер</label>
            <div class="view-value"><?= ( $model->idPartner ? Html::a("{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner]) : '') ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Дата платежу</label>
            <div class="view-value"><?= date('d.m.Y', strtotime($model->date_payment)) ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Тип</label>
            <div class="view-value"><?= (isset($paymentTypeList[$model->type_payment]) ? $paymentTypeList[$model->type_payment] : $model->type_payment ) ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Використано</label>
            <div class="view-value"><?= $percentage ?></div>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-3">
            <label class="view-label">Курс</label>
            <div class="view-value"><?= $model->value_kurs_payment ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Сума у USD</label>
            <div class="view-value"><?= $model->amount_usd_payment ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Сума у грн</label>
            <div class="view-value"><?= $model->amount_uah_payment ?></div>
        </div> 
        <div class="col-sm-3">
            <label class="view-label">Залишок</label>
            <div class="view-value"><?= round($model->amount_usd_payment - $outcomeAmount,2)  ?>&nbsp;USD</div>
        </div>
    </div>

    <div>
        <?php
        if ($outcome) {
            ?>
            <h4>Оплачені покупки</h4>
            <table class="table table-striped table-bordered">
                <tr>
                    <td style="width:150px;"></td>
                    <td>Товар</td>
                    <td>Кількість</td>
                    <td>Дата</td>
                    <td>Ціна, грн</td>
                    <td>Ціна,  USD</td>
                    <td>Курс,  грн/USD</td>
                </tr>
                <?php
                foreach ($outcome as $outcome) {
                    $buy = $outcome->idBuy;
                    ?>
                                <tr>
                                    <td style="width:150px;"><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/buy/view', 'id' =>$buy->id_buy ], ['class' => 'btn btn-xs btn-primary']) ?></td>
                                    <td><?= $buy->name_product ?></td>
                                    <td><?= $buy->amount_buy . '&nbsp' . $buy->idUnit->name_unit ?></td>
                                    <td><?= $buy->date_buy ?></td>
                                    <td><?= $outcome->amount_grn_spend_buy ?></td>
                                    <td><?= $outcome->amount_usd_spend_buy ?></td>
                                    <td><?= $outcome->value_kurs_spend_buy ?></td>
                                </tr>
                    <?php
                }
                ?>
            </table>
                <?php
            }
            ?>
    </div>
    
    <?php
    if(!$model->is_used){
        // sale
        ?><h4>
            Неоплачені покупки
            <?= Html::a('Нова покупка', ['/buy/create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success','target'=>'_blank'])?>
        </h4>
            
        <table class="table table-striped table-bordered">
            <tr>
                <td style="width:150px;"></td>
                <td>Дата</td>
                <td>Товар</td>
                <td>Ціна, грн</td>
                <td>Оплачено, грн</td>
                <td>Ціна,  USD</td>
                <td>Оплачено, USD</td>
            </tr>
        <?php
        
        foreach($unpaidBuys as $pm){
            if($pm['date_buy'] > $model->date_payment){
                $kurs = $pm['value_kurs_buy'];
            }else{
                $kurs = $model->value_kurs_payment;
            }
            ?>
            <tr>
                <td style="width:150px;"><?= Html::a('Оплатити', ['spendbuy', 'id_payment' =>$model->id_payment,'id_buy'=>$pm['id_buy']], ['class' => 'btn btn-xs btn-primary']) ?></td>
                <td><?= date('d.m.Y', strtotime($pm['date_buy'])) ?></td>
                <td><?=$pm['name_product'] ?></td>
                <td><?= round($kurs * ( $pm['price_usd_buy']  ), 2) ?> грн</td>
                <td><?= round($kurs * ( $pm['spendBuyPaid'] ), 2) ?> грн</td>
                <td><?= round( $pm['price_usd_buy'], 2 ) ?> USD</td>
                <td><?= round( $pm['spendBuyPaid'], 2 ) ?> USD</td>
            </tr>
            <?php
        }

        ?>
        </table>
        <?php
    }
    ?>
    
</div>
