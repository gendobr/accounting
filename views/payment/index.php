<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;
use app\models\Payment;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежі';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;



$paymentTypeList = (new Payment() )->getPaymentTypeList();
$yesNoOptions=['0'=>'Ні','1'=>'Так'];
        
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?>
    
        <?= Html::a('Створити платіж', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],
             ['class' => 'yii\grid\ActionColumn'],


            ['attribute' => 'id_payment', 'filterOptions' => ['class' => 'column-id'],],
            //'id_company',
            //'id_partner',
            //'date_payment',
            [
                'attribute' => 'id_partner',
                'label'=>'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'date_payment',
                'label'=>'Дата',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'date_payment',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y',strtotime($model->date_payment) );
                }
            ],
            // 'value_kurs_payment',
            ['attribute' => 'amount_uah_payment', 'filterOptions' => ['class' => 'column-id'],],
            // 'amount_usd_payment',
            // 'type_payment',
             [
                'attribute' => 'type_payment',
                'label'=>'Тип',
                'filter' => $paymentTypeList,
                'content' => function ($model, $key, $index, $column) use($paymentTypeList) {
                    return isset($paymentTypeList[$model['type_payment']]) ? $paymentTypeList[$model['type_payment']] : '';
                }
            ],       
            [
                'attribute' => 'is_used',
                'label' => 'Використано',
                'filter' => $yesNoOptions,
                'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
                    return isset($yesNoOptions[$model['is_used']]) ? $yesNoOptions[$model['is_used']] : '';
                }
            ],
        ],
    ]); ?>
</div>
