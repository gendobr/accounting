<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$company = $model->idCompany;
$this->title = 'Оплата продажів';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Платежі', 'url' => ['index', 'id_company' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Платіж: ' . $model->id_payment, 'url' => ['view', 'id' => $model->id_payment]];
$this->params['breadcrumbs'][] = $this->title;

$paymentTypeList = $model->getPaymentTypeList();



$percentage = '';

$incomes = $model->getIncomeSales()->all();
$incomeAmount = 0;
foreach ($incomes as $income) {
    $incomeAmount+=$income->amount_usd_income_sale;
}
$percentage = round(100 * $incomeAmount / $model->amount_usd_payment, 2) . "%, ".round($incomeAmount,2)." USD";
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Партнер</label>
            <div class="view-value"><?= ( $model->idPartner ? Html::a("{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner]) : '') ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Дата платежу</label>
            <div class="view-value"><?= date('d.m.Y', strtotime($model->date_payment)) ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Тип</label>
            <div class="view-value"><?= (isset($paymentTypeList[$model->type_payment]) ? $paymentTypeList[$model->type_payment] : $model->type_payment ) ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Використано</label>
            <div class="view-value"><?= $percentage ?></div>
        </div>

    </div>
    <div class="row">

        <div class="col-sm-3">
            <label class="view-label">Курс</label>
            <div class="view-value"><?= $model->value_kurs_payment ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Сума у USD</label>
            <div class="view-value"><?= $model->amount_usd_payment ?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Сума у грн</label>
            <div class="view-value"><?= $model->amount_uah_payment ?></div>
        </div> 
        <div class="col-sm-3">
            <label class="view-label">Залишок</label>
            <div class="view-value"><?= round($model->amount_usd_payment - $incomeAmount,2)  ?>&nbsp;USD</div>
        </div>
    </div>
    <?php
    if(!$model->is_used){
        // sale
        ?><h4>
            Неоплачені продажі
            <?= Html::a('Новий продаж', ['/sale/create', 'id_company' => $company->id_company], ['class' => 'btn btn-xs btn-success', 'target'=>'_blank']) ?>
        </h4>
            
        <table class="table table-striped table-bordered">
            <tr>
                <th style="width:100px;"></th>
                <th>Дата</th>
                <th>Товар</th>
                <th>Кількість</th>
                <th>Ціна, грн</th>
                <th>Оплачено, грн</th>
                <th>Ціна,  USD</th>
                <th>Оплачено, USD</th>
            </tr>
        <?php
        
        foreach($unpaidSales as $pm){
            if($pm['date_sale'] > $model->date_payment){
                $kurs = $pm['value_kurs_sale'];
            }else{
                $kurs = $model->value_kurs_payment;
            }
            ?>
            <tr>
                <td><?= Html::a('Оплатити', ['incomesale', 'id_payment' =>$model->id_payment,'id_sale'=>$pm['id_sale']], ['class' => 'btn btn-xs btn-primary']) ?></td>
                <td><?= date('d.m.Y', strtotime($pm['date_sale'])) ?></td>
                <td><?=$pm['name_product'] ?></td>
                <td><?= $pm['amount']. '&nbsp' . $pm['name_unit'] ?></td>
                <td><?= round($kurs * ( $pm['price_usd_sale']  ), 2) ?> грн</td>
                <td><?= round($kurs * ( $pm['incomeSalePaid'] ), 2) ?> грн</td>
                <td><?= round( $pm['price_usd_sale'], 2 ) ?> USD</td>
                <td><?= round( $pm['incomeSalePaid'], 2 ) ?> USD</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
    ?>
    <div>
        <?php
        if ($incomes) {
            ?>
            <h4>Оплачені продажі</h4>
            <table class="table table-striped table-bordered">
                <tr>
                    <th style="width:100px;"></th>
                    <th>Дата продажу</th>
                    <th>Товар</th>
                    <th>Кількість</th>
                    <th colspan="2">Надходження</th>
                </tr>
                    <?php
                    foreach ($incomes as $income) {
                        $sale = $income->idSale;
                        ?>
                                    <tr>
                                        <td><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/sale/view', 'id' => $sale->id_sale], ['class' => 'btn btn-xs btn-primary']) ?></td>
                                        <td><?= date('d.m.Y', strtotime($sale->date_sale)) ?></td>
                                        <td><?= $sale->name_product ?></td>
                                        <td><?= $sale->amount . '&nbsp' . $sale->idUnit->name_unit ?></td>
                                        <td><?= round($income->amount_uah_income_sale,2) ?> грн</td>
                                        <td><?= round($income->amount_usd_income_sale,2) ?> USD</td>
                                    </tr>
                        <?php
                    }
                    ?>
            </table>
                <?php
            }
            ?>
    </div>

</div>
