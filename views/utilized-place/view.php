<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\UtilizedPlace;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedPlace */
$company=$model->idCompany;

$this->title = $model->name_utilized_place;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Місця використання', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilized-place-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Змінити', ['update', 'id' => $model->id_utilized_place], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_utilized_place], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>
<?php

// $placePath = "<div> {$model->name_utilized_place} {$model->address_utilized_place} {$place->notes_utilized_place} </div>";
$placePath='';
$places = [];
$id_utilized_place_parent = $model->id_utilized_place_parent;
while ($id_utilized_place_parent && !in_array($id_utilized_place_parent, $places)) {
    $places[] = $id_utilized_place_parent;
    $place = UtilizedPlace::findOne($id_utilized_place_parent);
    $placePath .= "<div>@ {$place->name_utilized_place} {$place->address_utilized_place} {$place->notes_utilized_place} </div>";
    $id_utilized_place_parent = $place->id_utilized_place_parent;
}
?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_utilized_place',
            //'id_company',
            'name_utilized_place',
            'address_utilized_place',
            'notes_utilized_place',
            [
                'attribute' => 'id_utilized_place_parent',
                'label' =>'Є частиною',
                'value' => $placePath,
                'format'=>'html',
            ],
            [
                'attribute' => 'is_visible_utilized_place',
                'label' =>'Показувати',
                'value' => ($model->is_visible_utilized_place ? 'Так':'Ні' ),
            ],
        ],
    ]) ?>

</div>
