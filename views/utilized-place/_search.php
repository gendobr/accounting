<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedPlaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilized-place-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_utilized_place') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'name_utilized_place') ?>

    <?= $form->field($model, 'address_utilized_place') ?>

    <?= $form->field($model, 'notes_utilized_place') ?>

    <?php // echo $form->field($model, 'id_utilized_place_parent') ?>

    <?php // echo $form->field($model, 'is_visible_utilized_place') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
