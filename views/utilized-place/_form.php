<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\UtilizedPlace;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedPlace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilized-place-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_company')->label('')->hiddenInput() ?>

    <?= $form->field($model, 'name_utilized_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_utilized_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes_utilized_place')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'id_utilized_place_parent')->widget(Select2::classname(), [
        'data' => app\models\UtilizedPlace::listAll($model->id_company),
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Оберіть місце використання ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    
    ?>
    
    <?= $form->field($model, 'is_visible_utilized_place')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
