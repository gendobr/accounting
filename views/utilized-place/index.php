<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UtilizedPlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Місця використання';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilized-place-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Створити місце використання', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'id_utilized_place',
            // 'id_company',
            'name_utilized_place',
            'address_utilized_place',
            'notes_utilized_place',
            'id_utilized_place_parent',
            'is_visible_utilized_place',

            
        ],
    ]); ?>
</div>
