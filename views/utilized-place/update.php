<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedPlace */
$company=$model->idCompany;

$this->title = 'Місце використання: ' . $model->name_utilized_place;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Місця використання', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => $model->name_utilized_place, 'url' => ['view', 'id' => $model->id_utilized_place]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="utilized-place-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
