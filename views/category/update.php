<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */


$company=$model->idCompany;

$this->title = 'Змінити категорію: ' . $model->name_category;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Категорії', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => $model->name_category, 'url' => ['view', 'id' => $model->id_category]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
