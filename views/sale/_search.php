<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SaleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_sale') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_partner') ?>

    <?= $form->field($model, 'id_unit') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'value_kurs_sale') ?>

    <?php // echo $form->field($model, 'date_sale') ?>

    <?php // echo $form->field($model, 'name_product') ?>

    <?php // echo $form->field($model, 'price_uah_sale') ?>

    <?php // echo $form->field($model, 'price_usd_sale') ?>

    <?php // echo $form->field($model, 'price_per_unit_uah_sale') ?>

    <?php // echo $form->field($model, 'price_per_unit_usd_sale') ?>

    <?php // echo $form->field($model, 'discount_sale') ?>

    <?php // echo $form->field($model, 'is_paid_sale') ?>

    <?php // echo $form->field($model, 'id_invoice') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
