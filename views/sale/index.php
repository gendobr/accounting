<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Продажі';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;


$yesNoOptions = ['0' => 'Ні', '1' => 'Так'];
?>
<div class="sale-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Новий продаж', ['create', 'id_company' => $company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '
                    <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="{key}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Дії
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="{key}">
                      <li>{view}</li>
                      <li>{update}</li>
                      <li role="separator" class="divider"></li>
                      <li>{payment}</li>
                      <li role="separator" class="divider"></li>
                      <li>{delete}</li>
                    </ul>
                  </div>
                 ',
                // don't update if payment exists
                'buttons' => [
                    'key'=>function($url, $model, $key){
                        return 'dropdownMenu'.$key;
                    },
                    'update' => function ($url, $model, $key) {
                        //$payments = $model->getIncomeSales()->count();
                        //if($payments){
                        //    return '';                            
                        //}else{
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span> Змінити', ['update', 'id' => $model->id_sale], ['title' => "Змінити"]);
                        //}
                    },
                    'payment'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-piggy-bank"></span> Платежі', 
                                ['payment', 'id' => $model->id_sale], 
                                ['title' => "Платежі"]);                            
                    },
                    'view'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Переглянути', 
                                ['view', 'id' => $model->id_sale], 
                                ['title' => "Переглянути"]);
                    },
                    'delete'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span> Видалити', 
                                ['delete', 'id' => $model->id_sale], 
                                ['title' => "Видалити","data-method"=>"post"]);
                    },
                ]
            ],
                    ['attribute' => 'id_sale', 'filterOptions' => ['class' => 'column-id'],],
                    // 'id_company',
                    // 'id_product',
                    [
                        'attribute' => 'id_partner',
                        'label' => 'Партнер',
                        'filter' => $partnerOptions,
                        'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                            return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                        }
                    ],
                    [
                        'attribute' => 'date_sale',
                        'label' => 'Дата',
                        'filterOptions' => ['class' => 'column-id'],
                        // 'filter' => $unitOptions,
                        'filter' => \yii\jui\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_sale',
                            'language' => 'uk',
                            'dateFormat' => 'yyyy-MM-dd',
                        ]),
                        'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y', strtotime($model->date_sale));
                }
                    ],
                    //[
                    //    'attribute' => 'id_unit',
                    //    'label'=>'Одиниці вимірюванння',
                    //    'filter' => $unitOptions,
                    //    'content' => function ($model, $key, $index, $column) use($unitOptions) {
                    //        return isset($unitOptions[$model['id_unit']]) ? $unitOptions[$model['id_unit']] : '';
                    //    }
                    //],
                    'name_product',
                    [
                        'attribute' => 'amount',
                        'label' => 'Кількість',
                        'filterOptions' => ['class' => 'column-id'],
                        // 'filter' => $unitOptions,
                        'content' => function ($model, $key, $index, $column) use($unitOptions) {
                    return $model->amount . ' ' . ( isset($unitOptions[$model['id_unit']]) ? $unitOptions[$model['id_unit']] : '' );
                }
                    ],
                    //'value_kurs_sale',
                    // 'date_sale',
                    ['attribute' => 'price_uah_sale', 'filterOptions' => ['class' => 'column-id'],],
                    // 'price_usd_sale',
                    // 'price_per_unit_uah_sale',
                    // 'price_per_unit_usd_sale',
                    // 'discount_sale',
                    [
                        'attribute' => 'is_paid_sale',
                        'filterOptions' => ['class' => 'column-id'],
                        'label' => 'Оплачено',
                        'filter' => $yesNoOptions,
                        'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
                    return isset($yesNoOptions[$model['is_paid_sale']]) ? $yesNoOptions[$model['is_paid_sale']] : '';
                }
                    ],
                    'id_invoice',
                ],
            ]);
            ?>
</div>
