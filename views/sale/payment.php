<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sale */

$company = $model->idCompany;
$this->title = 'Платежі продажу ' . $model->id_sale;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Продажі', 'url' => ['index', 'id_company' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Продаж: ' . $model->id_sale, 'url' => ['view', 'id' => $model->id_sale]];
$this->params['breadcrumbs'][] = $this->title;


$payments = $model->getIncomeSales()->all();
?>
<div class="sale-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Партнер</label>
            <div class="view-value"><?=( $model->idPartner ? Html::a("{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner]) : '')?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Товар</label>
            <div class="view-value">
                <?=( $model->amount . "&nbsp;" . ( $model->idUnit ? ($model->idUnit->name_unit) : '') )?>,
                <?=( $model->id_product ? Html::a($model->name_product, ['/product/view', 'id' => $model->id_product]) : $model->name_product)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label class="view-label">Ціна</label>
            <div class="view-value">
                 <?=$model->price_uah_sale?> грн 
                (<?=$model->price_per_unit_uah_sale?> грн/<?=$model->idUnit->name_unit?>)
                |
                 <?=round($model->price_usd_sale, 2)?> USD 
                (<?=round($model->price_per_unit_usd_sale, 2)?> USD/<?=$model->idUnit->name_unit?>)
                |
                <?=$model->value_kurs_sale?> ГРН/USD
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Дата продажу</label>
            <div class="view-value"><?=date('d.m.Y', strtotime($model->date_sale))?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Знижка</label>
            <div class="view-value"><?=$model->discount_sale?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Повністю оплачено</label>
            <div class="view-value"><?=($model->is_paid_sale ? 'Так' : 'Ні' )?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Накладна №</label>
            <div class="view-value"><?=$model->id_invoice?></div>
        </div>
    </div>
    
    <div class="row">

        <div class="col-sm-12">
            <h4>Платежі</h4>
            <table class="table table-striped table-bordered">
                <?php
                foreach ($payments as $payment) {
                    $pm = $payment->idPayment;
                    ?>
                    <tr>
                        <td><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/payment/view', 'id' => $pm->id_payment], ['class' => '']) ?></td>
                        <td><?= $pm->id_payment ?></td>
                        <td><?= date('d.m.Y', strtotime($pm->date_payment)) ?></td>
                        <td><?= $payment->amount_uah_income_sale ?>&nbsp;грн</td>
                        <td><?= $payment->amount_usd_income_sale ?>&nbsp;USD</td>
                        <td><?= $payment->value_kurs_income_sale ?>&nbsp;грн/USD</td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
    
    <?php
    if(!$model->is_paid_sale){
        // sale
        ?><h4>Доступні аванси</h4>
            
        <table class="table table-striped table-bordered">
        <?php
        foreach($availablePayments as $pm){
            if($pm['type_payment']!='income'){
                continue;
            }
            ?>
            <tr>
                <td style="width:150px;"><?= Html::a('Використати', ['usepayment', 'id_payment' => $pm['id_payment'],'id_sale'=>$model->id_sale], ['class' => 'btn btn-xs btn-primary']) ?></td>
                <!-- <td><?= $pm['id_payment'] ?></td> -->
                <td><?= date('d.m.Y', strtotime($pm['date_payment'])) ?></td>
                <td><?= round( $pm['amount_usd_payment'] - $pm['incomeSalePaid'], 2 ) ?> USD</td>
                <td><?= ($kurs?$kurs->getUAHfromUSD( $pm['amount_usd_payment'] - $pm['incomeSalePaid'] ):'undefined') ?> грн</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
    ?>

</div>
