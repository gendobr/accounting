<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sale */

$company = $model->idCompany;
$this->title = 'Продаж ' . $model->id_sale;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Продажі', 'url' => ['index', 'id_company' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;


$payments = $model->getIncomeSales()->all();
?>
<div class="sale-view">

    <h1><?= Html::encode($this->title) ?>
        &nbsp;
        <?php /* ( count($payments)==0 ?Html::a('Змінити', ['update', 'id' => $model->id_sale], ['class' => 'btn btn-xs btn-primary']) :'' ) */ ?>
        <?= Html::a('Змінити', ['update', 'id' => $model->id_sale], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Платежі', ['payment', 'id' => $model->id_sale], ['class' => 'btn btn-xs btn-primary']) ?>
        <?=
        Html::a('Видалити', ['delete', 'id' => $model->id_sale], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ])
        ?>
    </h1>

    <div class="row">
        <div class="col-sm-6">
            <h4>Властивості</h4>
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id_sale',
                    // 'id_company',
                    // 'id_product',
                    // 'id_partner',
                    [
                        'attribute' => 'id_partner',
                        'label' => 'Партнер',
                        'format' => 'html',
                        'value' => ( $model->idPartner ? Html::a("{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner]) : ''),
                    ],
                    [
                        'attribute' => 'name_product',
                        'label' => 'Товар',
                        'format' => 'html',
                        'value' => ( $model->id_product ? Html::a($model->name_product, ['/product/view', 'id' => $model->id_product]) : $model->name_product),
                    ],
                    [
                        'label' => 'Кількість товару',
                        'format' => 'html',
                        'value' => ( $model->amount . "&nbsp;" . ( $model->idUnit ? ($model->idUnit->name_unit) : '') ),
                    ],
                    [
                        'attribute' => 'date_sale',
                        'label' => 'Дата продажу',
                        'value' => date('d.m.Y', strtotime($model->date_sale)),
                    ],
                    [
                        'label' => 'Ціна, грн',
                        'value' => "
                            {$model->price_uah_sale} грн, ({$model->price_per_unit_uah_sale} грн/{$model->idUnit->name_unit})
                            ",
                    ],
                    [
                        'label' => 'Ціна, USD',
                        'value' => "
                            {$model->price_usd_sale} USD, ({$model->price_per_unit_usd_sale} USD/{$model->idUnit->name_unit}, {$model->value_kurs_sale} грн/USD)
                            ",
                    ],
                    'discount_sale',
                    [
                        'attribute' => 'is_paid_sale',
                        'label' => 'Повністю оплачено ',
                        'value' => ($model->is_paid_sale ? 'Так' : 'Ні' ),
                    ],
                    'id_invoice',
                ],
            ])
            ?>
        </div>
        <div class="col-sm-6">
            <h4>Платежі</h4>
            <table class="table table-striped table-bordered">
                <?php
                foreach ($payments as $payment) {
                    $pm = $payment->idPayment;
                    ?>
                    <tr>
                        <td><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/payment/view', 'id' => $pm->id_payment], ['class' => '']) ?></td>
                        <td><?= $pm->id_payment ?></td>
                        <td><?= date('d.m.Y', strtotime($pm->date_payment)) ?></td>
                        <td><b><?= $payment->amount_usd_income_sale ?>&nbsp;USD</b></td>
                        <td><?= $payment->amount_uah_income_sale ?>&nbsp;грн</td>
                        <td><?php $kurs=$payment->kurs; echo $kurs?$kurs->value_kurs:'-'; ?>&nbsp;ГРН/USD</td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>

</div>
