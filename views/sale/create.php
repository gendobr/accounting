<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sale */


$this->title = 'Новий продаж';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Продажі', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
