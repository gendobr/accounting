<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IncomeSaleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="income-sale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_income_sale') ?>

    <?= $form->field($model, 'id_sale') ?>

    <?= $form->field($model, 'id_payment') ?>

    <?= $form->field($model, 'value_kurs_income_sale') ?>

    <?= $form->field($model, 'date_income_sale') ?>

    <?php // echo $form->field($model, 'amount_uah_income_sale') ?>

    <?php // echo $form->field($model, 'amount_usd_income_sale') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
