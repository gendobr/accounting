<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IncomeSale */

$this->title = 'Update Income Sale: ' . $model->id_income_sale;
$this->params['breadcrumbs'][] = ['label' => 'Income Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_income_sale, 'url' => ['view', 'id' => $model->id_income_sale]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="income-sale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
