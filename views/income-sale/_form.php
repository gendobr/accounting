<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IncomeSale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="income-sale-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_sale')->textInput() ?>

    <?= $form->field($model, 'id_payment')->textInput() ?>

    <?= $form->field($model, 'value_kurs_income_sale')->textInput() ?>

    <?= $form->field($model, 'date_income_sale')->textInput() ?>

    <?= $form->field($model, 'amount_uah_income_sale')->textInput() ?>

    <?= $form->field($model, 'amount_usd_income_sale')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
