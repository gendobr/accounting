<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeSaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Income Sales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="income-sale-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Income Sale', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_income_sale',
            'id_sale',
            'id_payment',
            'value_kurs_income_sale',
            'date_income_sale',
            // 'amount_uah_income_sale',
            // 'amount_usd_income_sale',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
