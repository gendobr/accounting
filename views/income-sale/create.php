<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IncomeSale */

$this->title = 'Create Income Sale';
$this->params['breadcrumbs'][] = ['label' => 'Income Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="income-sale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
