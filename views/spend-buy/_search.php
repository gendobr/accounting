<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpendBuySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="spend-buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_spend_buy') ?>

    <?= $form->field($model, 'id_payment') ?>

    <?= $form->field($model, 'id_buy') ?>

    <?= $form->field($model, 'amount_grn_spend_buy') ?>

    <?= $form->field($model, 'amount_usd_spend_buy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
