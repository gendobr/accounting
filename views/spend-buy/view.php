<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SpendBuy */

$this->title = $model->id_spend_buy;
$this->params['breadcrumbs'][] = ['label' => 'Spend Buys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spend-buy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_spend_buy], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_spend_buy], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_spend_buy',
            'id_payment',
            'id_buy',
            'amount_grn_spend_buy',
            'amount_usd_spend_buy',
        ],
    ]) ?>

</div>
