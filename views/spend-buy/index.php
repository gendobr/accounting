<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpendBuySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Spend Buys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spend-buy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Spend Buy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_spend_buy',
            'id_payment',
            'id_buy',
            'amount_grn_spend_buy',
            'amount_usd_spend_buy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
