<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SpendBuy */

$this->title = 'Create Spend Buy';
$this->params['breadcrumbs'][] = ['label' => 'Spend Buys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spend-buy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
