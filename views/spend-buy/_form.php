<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpendBuy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="spend-buy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_payment')->textInput() ?>

    <?= $form->field($model, 'id_buy')->textInput() ?>

    <?= $form->field($model, 'amount_grn_spend_buy')->textInput() ?>

    <?= $form->field($model, 'amount_usd_spend_buy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
