<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpendBuy */

$this->title = 'Update Spend Buy: ' . $model->id_spend_buy;
$this->params['breadcrumbs'][] = ['label' => 'Spend Buys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_spend_buy, 'url' => ['view', 'id' => $model->id_spend_buy]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="spend-buy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
