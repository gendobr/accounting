<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */


$company=$model->idCompany;

$this->title = 'Змінити партнера: ' . $model->name_partner;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Партнери', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => $model->name_partner, 'url' => ['view', 'id' => $model->id_partner]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
