<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */

$company=$model->idCompany;
$this->title = $model->name_partner;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Партнери', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Змінити', ['update', 'id' => $model->id_partner], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_partner], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

<table id="w0" class="table table-striped table-bordered detail-view">
    <tbody>
        <tr><th>ID партнера</th><td><?=$model->id_partner?></td></tr>
        <tr><th>Назва або ім'я</th><td><?=$model->name_partner?></td></tr>
        <tr><th>Контактна інформація</th><td><?=$model->kontakt_partner?></td></tr>
        <tr><th>Примітки</th><td><?=$model->note_partner?></td></tr>

        <tr><th>Неоплачених закупок</th><td><?=$kurs->getUAHfromUSD($buyBalance)?> грн</td></tr>
        <tr><th>Неоплачених продажів</th><td><?=$kurs->getUAHfromUSD($saleBalance)?> грн</td></tr>
        <tr><th>Авансові платежі</th><td></td></tr>
        <tr><td>Вхідні</td><td><?=$kurs->getUAHfromUSD($paymentBalance['income'])?> грн</td></tr>
        <tr><td>Вихідні</td><td><?=$kurs->getUAHfromUSD($paymentBalance['outcome'])?> грн</td></tr>
    </tbody></table>
</div>
