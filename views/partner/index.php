<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Партнери';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Створити партнера', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id_partner',
            ['attribute' => 'id_partner', 'filterOptions' => ['class' => 'column-id'],],
            'name_partner',
            'kontakt_partner',
            'note_partner',

        ],
    ]); ?>
</div>
