<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товари';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Створити товар', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?php
    
    $categoryOptions = app\models\Category::listAll($company->id_company);
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            ['class' => 'yii\grid\ActionColumn'],
            
            ['attribute' => 'id_product', 'filterOptions' => ['class' => 'column-id'],],
            // 'id_company',

            [
                'attribute' => 'id_category',
                'label'=>'Категорія',
                'filter' => $categoryOptions,
                'content' => function ($model, $key, $index, $column) use($categoryOptions) {
                    return isset($categoryOptions[$model['id_category']]) ? $categoryOptions[$model['id_category']] : '';
                }
            ],
            'id_unit',
            
            'name_product',
            // 'note_product',
            // ['attribute' => 'price_per_unit_usd_product', 'filterOptions' => ['class' => 'column-id'],],
            ['attribute' => 'is_visible_product', 'filterOptions' => ['class' => 'column-id'],],

            
        ],
    ]); ?>
</div>
