<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Unit;


/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_company')->label('')->hiddenInput() ?>

    <?php
    echo $form->field($model, 'id_category')->widget(Select2::classname(), [
        'data' => app\models\Category::listAll($model->id_company),
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Оберіть категорію ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    
    ?>

    <?php
    echo $form->field($model, 'id_unit')->widget(Select2::classname(), [
        'data' => Unit::listAll($model->id_company),
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Оберіть одиницю вимірювання ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    
    ?>

    <?php /* = $form->field($model, 'price_per_unit_usd_product')->textInput() */ ?>

    <?= $form->field($model, 'name_product')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note_product')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_visible_product')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
