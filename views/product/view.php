<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$company=$model->idCompany;

$this->title = $model->name_product;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Товари', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;

$unitOptions = app\models\Unit::listAll($company->id_company);
$categoryOptions=app\models\Category::listAll($company->id_company);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Змінити', ['update', 'id' => $model->id_product], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_product], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_product',
            // 'id_company',
            [
                'attribute' => 'id_category',
                'label' =>'Категорія',
                'value' => (isset($categoryOptions[$model->id_category]) ? $categoryOptions[$model->id_category] : ''),
            ],
            [
                'attribute' => 'id_unit',
                'label' =>'Одиниця вимірювання',
                'value' => (isset($unitOptions[$model->id_unit]) ? $unitOptions[$model->id_unit] : ''),
            ],
            // 'price_per_unit_usd_product',
            'name_product',
            'note_product',
            [
                'attribute' => 'is_visible_product',
                'label' =>'Видимість',
                'value' => ($model->is_visible_product ? 'Так' : 'Ні'),
            ],
        ],
    ]) ?>

</div>
