<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$company=$model->idCompany;

$this->title = 'Змінити товар: ' . $model->name_product;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Товари', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => $model->name_product, 'url' => ['view', 'id' => $model->id_product]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
