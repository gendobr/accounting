<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'id_category') ?>

    <?= $form->field($model, 'id_unit') ?>

    <?= $form->field($model, 'price_per_unit_usd_product') ?>

    <?php // echo $form->field($model, 'name_product') ?>

    <?php // echo $form->field($model, 'note_product') ?>

    <?php // echo $form->field($model, 'is_visible_product') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
