<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BuySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_buy') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'id_product') ?>

    <?= $form->field($model, 'id_partner') ?>

    <?= $form->field($model, 'value_kurs_buy') ?>

    <?php // echo $form->field($model, 'name_product') ?>

    <?php // echo $form->field($model, 'date_buy') ?>

    <?php // echo $form->field($model, 'amount_buy') ?>

    <?php // echo $form->field($model, 'id_unit') ?>

    <?php // echo $form->field($model, 'price_uah_buy') ?>

    <?php // echo $form->field($model, 'price_usd_buy') ?>

    <?php // echo $form->field($model, 'price_per_unit_usd_buy') ?>

    <?php // echo $form->field($model, 'price_per_unit_uah_buy') ?>

    <?php // echo $form->field($model, 'id_invoice') ?>

    <?php // echo $form->field($model, 'is_fully_paid_buy') ?>

    <?php // echo $form->field($model, 'is_fully_utilized_buy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
