<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Product;
use app\models\Partner;
use app\models\Unit;

/* @var $this yii\web\View */
/* @var $model app\models\Buy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_company')->label('')->hiddenInput() ?>

    <?php
    echo $form->field($model, 'id_product')->widget(Select2::classname(), [
        'data' => Product::listAll($model->id_company),
        'showToggleAll' => false,
        'options' => ['placeholder' => 'Оберіть товар ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    АБО
    <?= $form->field($model, 'name_product')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'id_partner')->widget(Select2::classname(), [
        'data' => Partner::listAll($model->id_company),
        'showToggleAll' => false,
        'options' => ['placeholder' => 'Оберіть партнера ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <div class="row">
        <div class="col-sm-3">
            <label class="control-label" for="buy-date_buy">Дата покупки</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_buy',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>

        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'amount_buy')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($model, 'id_unit')->widget(Select2::classname(), [
                'data' => Unit::listAll($model->id_company),
                'showToggleAll' => false,
                'options' => ['placeholder' => 'Оберіть одиницю вимірювання ...', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>        </div>
        <div class="col-sm-3">
             <?= $form->field($model, 'id_invoice')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'price_uah_buy')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'price_per_unit_uah_buy')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'is_fully_paid_buy')->checkbox() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'is_fully_utilized_buy')->checkbox() ?>
        </div>
    </div>




   







    


    

    

    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

        <?php ActiveForm::end(); ?>

</div>
