<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buy */

$company=$model->idCompany;
$this->title = 'Змінити покупку: ' . $model->id_buy;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупка: ' . $model->id_buy, 'url' => ['view', 'id' => $model->id_buy]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="buy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
