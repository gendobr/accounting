<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\UtilizedPlace;

/* @var $this yii\web\View */
/* @var $model app\models\Buy */

$company=$model->idCompany;
$payments=$model->getSpendBuys()->all();
$utilizations=$model->getUtilizedBuys()->all();

$this->title = 'Платежі покупки '.$model->id_buy;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупка: ' . $model->id_buy, 'url' => ['view', 'id' => $model->id_buy]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Партнер</label>
            <div class="view-value"><?=( $model->idPartner ? Html::a( "{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner ]): '')?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Товар</label>
            <div class="view-value"><?=( $model->id_product ? Html::a( $model->name_product, ['/product/view', 'id' => $model->id_product ]) : $model->name_product)?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Кількість товару</label>
            <div class="view-value"><?=(  $model->amount_buy ."&nbsp;".( $model->idUnit ? ($model->idUnit->name_unit) : '') )?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Дата покупки</label>
            <div class="view-value"><?=date('d.m.Y',strtotime($model->date_buy) )?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Ціна</label>
            <div class="view-value"><?=$model->price_uah_buy?> грн</div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">&nbsp;</label>
            <div class="view-value"><?=$model->price_usd_buy?> USD</div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Ціна за одницю</label>
            <div class="view-value"><?=$model->price_per_unit_uah_buy?>грн</div>
        </div>
        <div class="col-sm-3">
            <label class="view-label"></label>
            <div class="view-value"><?=$model->price_per_unit_usd_buy?> USD</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="view-label">Курс</label>
            <div class="view-value"><?=$model->value_kurs_buy?> грн/USD</div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Накладна №</label>
            <div class="view-value"><?=$model->id_invoice?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Повністю оплачено</label>
            <div class="view-value"><?=($model->is_fully_paid_buy ? '<span class="btn btn-success" style="cursor:default">Так</span>':'<span class="btn btn-danger" style="cursor:default">Ні</span>' )?></div>
        </div>
        <div class="col-sm-3">
            <label class="view-label">Повністю використано</label>
            <div class="view-value"><?=($model->is_fully_utilized_buy ? '<span class="btn btn-success" style="cursor:default">Так</span>':'<span class="btn btn-danger" style="cursor:default">Ні</span>' )?></div>
        </div>
    </div>


    
    <div class="row">
        <div class="col-sm-12">
            <h4>Платежі</h4>
            <table class="table table-striped table-bordered">
            <?php
            foreach($payments as $payment){
                $pm=$payment->idPayment;
                ?>
                <tr>
                    <td><?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/payment/view', 'id' => $pm->id_payment], ['class' => '']) ?></td>
                    <td><?=$pm->id_payment?></td>
                    <td><?=date('d.m.Y',strtotime($pm->date_payment))?></td>
                    <td><?=$payment->amount_grn_spend_buy?>&nbsp;грн</td>
                    <td><?=$payment->amount_usd_spend_buy?>&nbsp;USD</td>
                    <td><?=$payment->value_kurs_spend_buy?>&nbsp;грн/USD</td>
                </tr>
                <?php
                
                
                
            }
            ?>
            </table>
        </div>
        
    </div>
    

    <?php
    if(!$model->is_fully_paid_buy){
        // sale
        ?><h4>Доступні аванси</h4>
            
        <table class="table table-striped table-bordered">
        <?php
        
        // $kurs = Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();
        foreach($availablePayments as $pm){
            if($pm['type_payment']!='outcome'){
                continue;
            }
            ?>
            <tr>
                <td style="width:150px;"><?= Html::a('Використати', ['usepayment', 'id_payment' => $pm['id_payment'],'id_buy'=>$model->id_buy], ['class' => 'btn btn-xs btn-primary']) ?></td>
                <!-- <td><?= $pm['id_payment'] ?></td> -->
                <td><?= date('d.m.Y', strtotime($pm['date_payment'])) ?></td>
                <td><?= $kurs->getUAHfromUSD( $pm['amount_usd_payment'] - $pm['spendBuyPaid'] ) ?> грн</td>
                <td><?= round( $pm['amount_usd_payment'] - $pm['spendBuyPaid'], 2 ) ?> USD</td>
            </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
    ?>
</div>
