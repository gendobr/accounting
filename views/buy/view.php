<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\UtilizedPlace;

/* @var $this yii\web\View */
/* @var $model app\models\Buy */

$company=$model->idCompany;
$payments=$model->getSpendBuys()->all();
$utilizations=$model->getUtilizedBuys()->all();

$this->title = 'Покупка '.$model->id_buy;
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Змінити', ['update', 'id' => $model->id_buy], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Використання', ['/utilized-buy/index', 'id_buy' => $model->id_buy], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Платежі', ['/buy/payment', 'id' => $model->id_buy], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_buy], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно бажаєте видалити цей запис?',
                'method' => 'post',
            ],
        ]) ?>
   </h1>

<table id="w0" class="table table-striped table-bordered detail-view"><tbody>
    <tr><th>Товар</th>
        <td><?=( $model->id_product ? Html::a( $model->name_product, ['/product/view', 'id' => $model->id_product ]) : $model->name_product)?></td>
        <th>Партнер</th>
        <td><?=( $model->idPartner ? Html::a( "{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner ]): '')?></td>
    </tr>
    <tr>
        <th>Кількість товару</th>
        <td><?=(  $model->amount_buy ."&nbsp;".( $model->idUnit ? ($model->idUnit->name_unit) : '') )?></td>
        <th>Дата покупки</th>
        <td><?=date('d.m.Y',strtotime($model->date_buy) )?></td>
    </tr>
    <tr>
        <th>Ціна, грн</th><td><?=round($model->price_uah_buy,2)?></td>
        <th>Ціна, USD</th><td><?=round($model->price_usd_buy,2)?></td>
    </tr>
    <tr>
        <th>Ціна за одиницю, грн</th><td><?=round($model->price_per_unit_uah_buy,2)?></td>
        <th>Ціна за одиницю, USD</th><td><?=round($model->price_per_unit_usd_buy,2)?></td>
    </tr>
    <tr>
        <th>Курс, грн/USD</th><td><?=round($model->value_kurs_buy,2)?></td>
        <th>Накладна №</th><td><?=$model->id_invoice?></td>
    </tr>
    <tr>
        <th>Повністю оплачено </th><td><?=($model->is_fully_paid_buy ? 'Так':'Ні' )?></td>
        <th>Повністю використано</th><td><?=($model->is_fully_utilized_buy ? 'Так':'Ні' )?></td>
    </tr>
</tbody></table>
    
    
    
    <?php
    /*
    = DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_buy',
            //'id_company',
            //'id_product',
            //'id_partner',
            //'name_product',
            //            [
            //                'attribute' => 'id_partner',
            //                'label' =>'Партнер',
            //                'format'=>'html',
            //                'value' => ( $model->idPartner ? Html::a( "{$model->idPartner->name_partner} {$model->idPartner->kontakt_partner}", ['/partner/view', 'id' => $model->id_partner ]): ''),
            //            ],
            //            [
            //                'attribute' => 'name_product',
            //                'label' =>'Товар',
            //                'format'=>'html',
            //                'value' => ( $model->id_product ? Html::a( $model->name_product, ['/product/view', 'id' => $model->id_product ]) : $model->name_product),
            //            ],
            //            [
            //                'label' =>'Кількість товару',
            //                'format'=>'html',
            //                'value' => (  $model->amount_buy ."&nbsp;".( $model->idUnit ? ($model->idUnit->name_unit) : '') ),
            //            ],
            //            [
            //                'attribute' => 'date_buy',
            //                'label' =>'Дата покупки',
            //                'value' => date('d.m.Y',strtotime($model->date_buy) ),
            //            ],
            //            [
            //                'attribute' => 'price_uah_buy',
            //                'label' =>'Ціна, грн',
            //                'value' => round($model->price_uah_buy,2),
            //            ],
            //            [
            //                'attribute' => 'price_usd_buy',
            //                'label' =>'Ціна, USD',
            //                'value' => round($model->price_usd_buy,2),
            //            ],
            //            [
            //                'attribute' => 'price_per_unit_uah_buy',
            //                'label' =>'Ціна за одиницю, грн',
            //                'value' => round($model->price_per_unit_uah_buy,2),
            //            ],
            //            [
            //                'attribute' => 'price_per_unit_usd_buy',
            //                'label' =>'Ціна за одиницю, USD',
            //                'value' => round($model->price_per_unit_usd_buy,2),
            //            ],
            // 'id_unit',
            //            [
            //                'attribute' => 'value_kurs_buy',
            //                'label' =>'Курс, грн/USD',
            //                'value' => round($model->value_kurs_buy,2),
            //            ],     
            //            'id_invoice',
            [
                'attribute' => 'is_fully_paid_buy',
                'label' =>'Повністю оплачено ',
                'value' => ($model->is_fully_paid_buy ? 'Так':'Ні' ),
            ],            
            [
                'attribute' => 'is_fully_utilized_buy',
                'label' =>'Повністю використано',
                'value' => ($model->is_fully_utilized_buy ? 'Так':'Ні' ),
            ],
        ],
    ]) */
    ?>

    
    <div class="row">
        <div class="col-sm-6">
            <h4>Платежі</h4>
            <table class="table table-striped table-bordered">
            <?php
            foreach($payments as $payment){
                $pm=$payment->idPayment;
                ?>
                <tr>
                    <td><?= $pm->id_payment?></td>
                    <td><?= date('d.m.Y',strtotime($pm->date_payment))?></td>
                    <td><?= $payment->amount_grn_spend_buy?>&nbsp;грн</td>
                    <td><?= $payment->amount_usd_spend_buy ?>&nbsp;USD</td>
                    <td><?= $payment->value_kurs_spend_buy ?>&nbsp;грн/USD</td>
                </tr>
                <?php
                
                
                
            }
            ?>
            </table>
        </div>
        <div class="col-sm-6">
            <h4>Використання</h4>
            <table class="table table-striped table-bordered">
            <?php
            foreach($utilizations as $utilization){
                ?>
                <tr>
                    <td><?=date('d.m.Y',strtotime($utilization->date_utilized_buy))?></td>
                    <td><?=($utilization->amount_utilized_buy." ".$utilization->idBuy->idUnit->name_unit)?></td>
                    <td><?php
                    $place=$utilization->idUtilizedPlace;
                    echo "<div> {$place->name_utilized_place} {$place->address_utilized_place} {$place->notes_utilized_place} </div>";
                    
                    $places=[];
                    $id_utilized_place_parent=$place->id_utilized_place_parent;
                    while($id_utilized_place_parent && !in_array($id_utilized_place_parent, $places)){
                        $places[]=$id_utilized_place_parent;
                        $place=  UtilizedPlace::findOne($id_utilized_place_parent);
                        echo "<div>@ {$place->name_utilized_place} {$place->address_utilized_place} {$place->notes_utilized_place} </div>";
                        $id_utilized_place_parent=$place->id_utilized_place_parent;
                    }

                    ?></td>
                </tr>
                <?php
                //
                //
                //id_utilized_place
            }
            ?>
            </table>
        </div>
    </div>
    
    
</div>
