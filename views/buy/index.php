<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BuySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Покупки';
$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;


$yesNoOptions=['0'=>'Ні','1'=>'Так'];
?>
<div class="buy-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Нова покупка', ['create', 'id_company'=>$company->id_company], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',         
                'template' => '
                    <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="{key}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Дії
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="{key}">
                      <li>{view}</li>
                      <li>{update}</li>
                      <li>{utilized}</li>
                      <li>{payment}</li>
                      <li role="separator" class="divider"></li>
                      <li>{delete}</li>
                    </ul>
                  </div>
                 ',
                // don't update if payment exists
                'buttons' => [
                    'key'=>function($url, $model, $key){
                        return 'dropdownMenu'.$key;
                    },
                    'utilized'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon  glyphicon-send"></span> Використання', 
                                       ['/utilized-buy/index', 'id_buy' => $model->id_buy], 
                                       ['title' => "Використання"]);
                    },
                    'update' => function ($url, $model, $key) {
                        //$payments = $model->getSpendBuys()->count();
                        //if($payments){
                        //    return '';                            
                        //}else{
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span> Змінити', ['update', 'id' => $model->id_buy], ['title' => "Змінити"]);
                        //}
                    },
                    'payment'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-piggy-bank"></span> Платежі', 
                                ['payment', 'id' => $model->id_buy], 
                                ['title' => "Платежі"]);                            
                    },
                    'view'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Переглянути', 
                                ['view', 'id' => $model->id_buy], 
                                ['title' => "Переглянути"]);
                    },
                    'delete'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span> Видалити', 
                                ['delete', 'id' => $model->id_buy], 
                                ['title' => "Видалити","data-method"=>"post"]);
                    },
                ]
                
            ],
            ['attribute' => 'id_buy', 'filterOptions' => ['class' => 'column-id'],],
            // 'id_company',
            // 'id_product',
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'date_buy',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_buy',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y', strtotime($model->date_buy));
                }
            ],
            'name_product',
            [
                'attribute' => 'amount_buy',
                'label' => 'Кількість',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'content' => function ($model, $key, $index, $column) use($unitOptions) {
            return $model->amount_buy . ' ' . ( isset($unitOptions[$model['id_unit']]) ? $unitOptions[$model['id_unit']] : '' );
        }
            ],
            ['attribute' => 'price_uah_buy', 'filterOptions' => ['class' => 'column-id'],],
                    
                    
            [
                'attribute' => 'is_fully_paid_buy',
                'label' => 'Оплачено',
                'filter' => $yesNoOptions,
                'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
                    return isset($yesNoOptions[$model['is_fully_paid_buy']]) ? $yesNoOptions[$model['is_fully_paid_buy']] : '';
                }
            ],
            //['attribute' => 'is_fully_paid_buy', 'filterOptions' => ['class' => 'column-id'],],
            [
            'attribute' => 'is_fully_utilized_buy',
            'label' => 'Використано',
            'filter' => $yesNoOptions,
            'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
                return isset($yesNoOptions[$model['is_fully_utilized_buy']]) ? $yesNoOptions[$model['is_fully_utilized_buy']] : '';
            }
        ],
        //['attribute' => 'is_fully_utilized_buy', 'filterOptions' => ['class' => 'column-id'],],
            'id_invoice',
            //  'value_kurs_buy',
            // 'name_product',
            // 'date_buy',
            // 'amount_buy',
            // 'id_unit',
            // 'price_uah_buy',
            // 'price_usd_buy',
            // 'price_per_unit_usd_buy',
            // 'price_per_unit_uah_buy',            
            // 'is_fully_paid_buy',
            // 'is_fully_utilized_buy',
            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
