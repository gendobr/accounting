<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UtilizedBuySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Використання покупок';

$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['/buy/index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупка: ' . $buy->id_buy, 'url' => ['/buy/view', 'id' => $buy->id_buy]];
$this->params['breadcrumbs'][] = $this->title;


// var_dump($searchModel->places);
?>
<div class="utilized-buy-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Зареєструвати використання', ['create', 'id_buy' => $buy->id_buy], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
    
    <h3>
    <?=date('d.m.Y',strtotime($buy->date_buy))?>,
    <?=$buy->idPartner->name_partner?>,
    <?=$buy->name_product?>,
    <?=$buy->amount_buy?>
    <?=$buy->idUnit->name_unit?>,
    <?=$buy->price_uah_buy?> грн
    </h3>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
            ['attribute' => 'id_utilized_buy', 'filterOptions' => ['class' => 'column-id'],],
            // 'id_buy',
            // 'id_utilized_place',
            [
                'attribute' => 'id_utilized_place',
                'label' => 'Місце використання',
                'content' => function ($model, $key, $index, $column) use($searchModel) {
                    if(isset($searchModel->places[$model->id_utilized_place])){
                        $place=$searchModel->places[$model->id_utilized_place];
                        $path=[];
                        // $path[]=$place;
                        $leaf = $place;
                        $parentId = $place['id_utilized_place_parent'];
                        while( $parentId ){
                            if(isset($searchModel->places[$parentId])){
                                $place = $searchModel->places[$parentId];
                                $parentId = $place['id_utilized_place_parent'];
                                $path[]=$place;
                            }else{
                                $parentId=false;
                            }
                        }
                        $html=[];
                        // $path = array_reverse($path);
                        $html[] = "<div><b>{$leaf['name_utilized_place']} {$leaf['address_utilized_place']}</b></div>";
                        foreach($path as $pa){
                            $html[] = "<div>@{$pa['name_utilized_place']} {$pa['address_utilized_place']}</div>";
                        }
                        return join("",$html);
                    }else{
                        return $model->id_utilized_place;
                    }
                }
            ],
            [
                'attribute' => 'date_utilized_buy',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_utilized_buy',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y', strtotime($model->date_utilized_buy));
                }
            ],
            'amount_utilized_buy',

        ],
    ]); ?>
</div>
