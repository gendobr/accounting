<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UtilizedBuy */

$this->title = 'Зареєструвати використання';

$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['/buy/index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупка: ' . $buy->id_buy, 'url' => ['/buy/view', 'id' => $buy->id_buy]];

$this->params['breadcrumbs'][] = ['label' => 'Використання покупок', 'url' => ['index','id_buy'=>$buy->id_buy]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilized-buy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'buy'=>$buy,
        'company'=>$company
    ]) ?>

</div>
