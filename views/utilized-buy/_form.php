<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UtilizedPlace;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\UtilizedBuy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilized-buy-form">
<?php
  if($error=Yii::$app->session->getFlash('error')){
      ?><div class="alert alert-danger" role="alert"><?=$error?></div><?php
  }
?>
    <?php $form = ActiveForm::begin(); ?>

    <h3>
    <?=date('d.m.Y',strtotime($buy->date_buy))?>,
    <?=$buy->idPartner->name_partner?>,
    <?=$buy->name_product?>,
    <?=$buy->amount_buy?>
    <?=$buy->idUnit->name_unit?>,
    <?=$buy->price_uah_buy?> грн
    </h3>
    
    
    <?= $form->field($model, 'id_buy')->label('')->hiddenInput() ?>

   
    <?php
    echo $form->field($model, 'id_utilized_place')->widget(Select2::classname(), [
        'data' => UtilizedPlace::listAll($buy->id_company, $model->isNewRecord),
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Оберіть місце використання ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    
    ?>
        <div class="row">
            <div class="col-sm-6">
                <label class="control-label" for="buy-date_utilized_buy">Дата використання</label>
                <?php
                echo yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'date_utilized_buy',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => [
                        'class' => 'form-control'
                    ]
                ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'amount_utilized_buy')->textInput() ?>
            </div>
        </div>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
