<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedBuySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilized-buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_utilized_buy') ?>

    <?= $form->field($model, 'id_buy') ?>

    <?= $form->field($model, 'id_utilized_place') ?>

    <?= $form->field($model, 'date_utilized_buy') ?>

    <?= $form->field($model, 'amount_utilized_buy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
