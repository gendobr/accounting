<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedBuy */

$this->title = 'Запис про використання покупки: ' . $model->id_utilized_buy;

$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупки', 'url' => ['index', 'id_company'=>$company->id_company]];
$this->params['breadcrumbs'][] = ['label' => 'Покупка: ' . $buy->id_buy, 'url' => ['view', 'id' => $buy->id_buy]];
$this->params['breadcrumbs'][] = ['label' => 'Використання покупок', 'url' => ['index','id_buy'=>$buy->id_buy]];
$this->params['breadcrumbs'][] = ['label' => $model->id_utilized_buy, 'url' => ['view', 'id' => $model->id_utilized_buy]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="utilized-buy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
                'buy'=>$buy,
                'company'=>$company,
    ]) ?>

</div>
