<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizedBuy */

$this->title = $model->id_utilized_buy;
$this->params['breadcrumbs'][] = ['label' => 'Utilized Buys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilized-buy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_utilized_buy], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_utilized_buy], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_utilized_buy',
            'id_buy',
            'id_utilized_place',
            'date_utilized_buy',
            'amount_utilized_buy',
        ],
    ]) ?>

</div>
