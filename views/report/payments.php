<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\jui\DatePicker;
use kartik\widgets\Select2;

$this->title = 'Платежі';
$this->params['breadcrumbs'][] = ['label' => 'Звіти', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['payments'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_partner')->widget(Select2::classname(), [
                'data' => $partnerOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_company')->widget(Select2::classname(), [
                'data' => $companyOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата від</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_payment_min',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>
        
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата до</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_payment_max',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>

        
        
        
    </div>
    <div class="row">
        
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'type_payment')->widget(Select2::classname(), [
                'data' => $typePaymentOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'is_used')->widget(Select2::classname(), [
                'data' => $yesNoOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
           <?= $form->field($searchModel, 'id_payment') ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Знайти', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистити', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
    
    
    

    <?php ActiveForm::end(); ?>

</div>


<div class="buy-index">

    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id_payment', 
                'label' => 'ID платежу',
                'filterOptions' => ['class' => 'column-id'],
            ],
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'id_company',
                'label' => 'Підприємство',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($companyOptions) {
                    return isset($companyOptions[$model['id_company']]) ? $companyOptions[$model['id_company']] : '';
                }
            ],
            [
                'attribute' => 'type_payment',
                'label' => 'Тип',
                'filter' => $typePaymentOptions,
                'content' => function ($model, $key, $index, $column) use($typePaymentOptions) {
                    return isset($typePaymentOptions[$model['type_payment']]) ? $typePaymentOptions[$model['type_payment']] : '';
                }
            ],
            [
                'attribute' => 'date_payment',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y', strtotime($model['date_payment']));
                }
            ],
            [
                'attribute' => 'amount_uah_payment',
                'label' => 'Кількість',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'content' => function ($model, $key, $index, $column) {
                    return $model['amount_uah_payment'] . '&nbsp;грн';
                }
            ],
                    
            [
                'attribute' => 'amount_usd_payment',
                'label' => 'Кількість, USD',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'content' => function ($model, $key, $index, $column) {
                    return $model['amount_usd_payment'] . '&nbsp;USD';
                }
            ],
                    
            //            [
            //                'attribute' => 'is_used',
            //                'label' => 'Використано',
            //                //            'filter' => $yesNoOptions,
            //                'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
            //                    return isset($yesNoOptions[$model['is_used']]) ? $yesNoOptions[$model['is_used']] : '';
            //                }
            //            ],
            //
            [
                //'attribute' => 'is_fully_paid_buy',
                'label' => 'Використано',
                'content' => function ($searchModel, $key, $index, $column) {
                    switch($searchModel['type_payment']){
                        case 'income':
                            return round(100*$searchModel['incomeSalePaid'],2)."%";
                            break;
                        case 'outcome':
                            return round(100*$searchModel['spendBuyPaid'],2)."%";
                            break;
                    }
                }
            ],

                    
                    
//        'name_product',
                //            ],
                //            ['attribute' => 'price_uah_buy', 'filterOptions' => ['class' => 'column-id'],],
                //                    
                //                    
                //            //['attribute' => 'is_fully_paid_buy', 'filterOptions' => ['class' => 'column-id'],],
                //        //['attribute' => 'is_fully_utilized_buy', 'filterOptions' => ['class' => 'column-id'],],
                //            'id_invoice',
                //            //  'value_kurs_buy',
                //            // 'name_product',
                //            // 'date_buy',
                //            // 'amount_buy',
                //            // 'id_unit',
                //            // 'price_uah_buy',
                //            // 'price_usd_buy',
                //            // 'price_per_unit_usd_buy',
                //            // 'price_per_unit_uah_buy',            
                //            // 'is_fully_paid_buy',
                //            // 'is_fully_utilized_buy',
                //            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
