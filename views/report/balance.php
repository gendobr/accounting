<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\jui\DatePicker;
use kartik\widgets\Select2;

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = ['label' => 'Звіти', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['balance'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?php
            echo $form->field($searchModel, 'id_partner')->label('Партнер')->widget(Select2::classname(), [
                'data' => $partnerOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            echo $form->field($searchModel, 'id_company')->widget(Select2::classname(), [
                'data' => $companyOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-4">
            <label class="control-label" for="sale-date_sale">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Знайти', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистити', ['class' => 'btn btn-default']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="buy-index">

    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id_company',
                'label' => 'Підприємство',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($companyOptions) {
                    return isset($companyOptions[$model['id_company']]) ? $companyOptions[$model['id_company']] : '';
                }
            ],
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                }
            ],

            [
                'attribute' => 'paymentInAmount',
                'label' => 'Отримано',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['paymentInAmount']) . '&nbsp;грн';
                }
            ],
            [
                'attribute' => 'saleAmount',
                'label' => 'Продажів на суму',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['saleAmount']) . '&nbsp;грн';
                }
            ],
            [
                'attribute' => 'saleBalance',
                'label' => 'Аванс',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['saleBalance']) . '&nbsp;грн';
                }
            ],
                    
                    
                    
            [
                'attribute' => 'paymentOutAmount',
                'label' => 'Виплачено',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['paymentOutAmount']) . '&nbsp;грн';
                }
            ],
            [
                'attribute' => 'buyAmount',
                'label' => 'Закупок на суму',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['buyAmount']) . '&nbsp;грн';
                }
            ],
            [
                'attribute' => 'buyBalance',
                'label' => 'Борг',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($kurs) {
                    return $kurs->getUAHfromUSD($model['buyBalance']) . '&nbsp;грн';
                }
            ],
                    

        ],
    ]);
    ?>
</div>
