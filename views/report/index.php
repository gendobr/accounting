<?php

use yii\helpers\Html;

$this->title = 'Звіти';
//$this->params['breadcrumbs'][] = ['label' => 'Підприємства', 'url' => ['/company/index']];
//$this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul>
    <li><?= Html::a('Платежі', ['payments'], ['class' => '']) ?></li>
    <li><?= Html::a('Баланс', ['balance'], ['class' => '']) ?></li>
    <li><?= Html::a('Використання покупок', ['usage'], ['class' => '']) ?></li>
    <?php
    foreach($companyOptions as $key=>$val){
        ?><li><?=$val?>
        <div>
            <?= Html::a('Продажі', ['sales','id_company'=>$key], ['class' => '']) ?>
            <?= Html::a('Пoкупки', ['buys','id_company'=>$key], ['class' => '']) ?>
        </div>
         </li><?php
    }
    ?>
</ul>