<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\jui\DatePicker;
use kartik\widgets\Select2;

$this->title = 'Продажі';
$this->params['breadcrumbs'][] = ['label' => 'Звіти', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $company->name_company;
$this->params['breadcrumbs'][] = $this->title;


$yesNoOptions = ['0' => 'Ні', '1' => 'Так'];


?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="sale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['sales'],
        'method' => 'get',
    ]); ?>
    <input type="hidden" name="id_company" value="<?=$company->id_company?>">
    
    <div class="row">
        <div class="col-sm-3"><?= $form->field($searchModel, 'id_sale') ?></div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_partner')->widget(Select2::classname(), [
                'data' => $partnerOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_product')->widget(Select2::classname(), [
                'data' => $productOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'is_paid_sale')->label('Оплачено повністю')
                    ->widget(Select2::classname(), [
                'data' => $yesNoOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата продажу від</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_sale_min',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата продажу до</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_sale_max',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>            
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($searchModel, 'id_invoice') ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Знайти', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистити', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        
    </div>

    <?php // echo $form->field($searchModel, 'amount') ?>

    <?php // echo $form->field($searchModel, 'value_kurs_sale') ?>

    <?php // echo $form->field($searchModel, 'date_sale') ?>

    <?php // echo $form->field($searchModel, 'name_product') ?>

    <?php // echo $form->field($searchModel, 'price_uah_sale') ?>

    <?php // echo $form->field($searchModel, 'price_usd_sale') ?>

    <?php // echo $form->field($searchModel, 'price_per_unit_uah_sale') ?>

    <?php // echo $form->field($searchModel, 'price_per_unit_usd_sale') ?>

    <?php // echo $form->field($searchModel, 'discount_sale') ?>

    <?php // echo $form->field($searchModel, 'is_paid_sale') ?>

    



    <?php ActiveForm::end(); ?>

</div>
<div class="sale-index">

    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_sale',
                'label' => 'ID продажу',
                'filterOptions' => ['class' => 'column-id'],
            ],
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($searchModel, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$searchModel['id_partner']]) ? $partnerOptions[$searchModel['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'date_sale',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_sale',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'content' => function ($searchModel, $key, $index, $column) {
                    return date('d.m.Y', strtotime($searchModel['date_sale']));
                }
            ],
            [
                'attribute' => 'name_product',
                'label' => 'Товар',
            ],
            [
                'attribute' => 'amount',
                'label' => 'Кількість',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($searchModel, $key, $index, $column) use($unitOptions) {
                    return $searchModel['amount'] . ' ' . ( isset($unitOptions[$searchModel['id_unit']]) ? $unitOptions[$searchModel['id_unit']] : '' );
                }
            ],
            //'value_kurs_sale',
            // 'date_sale',
            [
                'attribute' => 'price_uah_sale', 
                'label' => 'Ціна, грн',
                'filterOptions' => ['class' => 'column-id'],
            ],
            [
                'attribute' => 'price_usd_sale', 
                'label' => 'Ціна, USD',
                'filterOptions' => ['class' => 'column-id'],
            ],      
            // 'price_usd_sale',
            // 'price_per_unit_uah_sale',
            // 'price_per_unit_usd_sale',
            // 'discount_sale',
            //            [
            //                'attribute' => 'is_paid_sale',
            //                'filterOptions' => ['class' => 'column-id'],
            //                'label' => 'Оплачено',
            //                'content' => function ($searchModel, $key, $index, $column) use($yesNoOptions) {
            //                    return isset($yesNoOptions[$searchModel['is_paid_sale']]) ? $yesNoOptions[$searchModel['is_paid_sale']] : '';
            //                }
            //            ],
            [
                'attribute' => 'partPaid',
                'filterOptions' => ['class' => 'column-id'],
                'label' => 'Оплачено',
                'content' => function ($searchModel, $key, $index, $column) use($yesNoOptions) {
                    return round(100*$searchModel['partPaid'],2)."%";
                }
            ],
            [
                'attribute' => 'id_invoice', 
                'label' => 'Накладна №',
            ],
        ],
    ]);
    ?>
</div>
