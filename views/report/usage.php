<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\jui\DatePicker;
use kartik\widgets\Select2;

$this->title = 'Використання покупок';
$this->params['breadcrumbs'][] = ['label' => 'Звіти', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
// $this->params['breadcrumbs'][] = $company->name_company;
$this->params['breadcrumbs'][] = $this->title;





?>

<h1><?= Html::encode($this->title) ?></h1>



<div class="sale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['usage'],
        'method' => 'get',
    ]); ?>
    
    <div class="row">
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_company')->widget(Select2::classname(), [
                'data' => $companyOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_partner')->label('Партнер')->widget(Select2::classname(), [
                'data' => $partnerOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата від</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_utilized_buy_min',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">Дата продажу до</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_utilized_buy_max',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>            
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->field($searchModel, 'place') ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($searchModel, 'id_invoice') ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($searchModel, 'name_product') ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Знайти', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистити', ['class' => 'btn btn-default']) ?>
            </div>
        </div>

    </div>



    <?php ActiveForm::end(); ?>

</div>

<div class="sale-index">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id_invoice',
                'label' => 'Накладна №',
                'filterOptions' => ['class' => 'column-id'],
            ],
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($searchModel, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$searchModel['id_partner']]) ? $partnerOptions[$searchModel['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'date_utilized_buy',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($searchModel, $key, $index, $column) {
                    return date('d.m.Y', strtotime($searchModel['date_utilized_buy']));
                }
            ],
            [
                'attribute' => 'name_product',
                'label' => 'Товар',
            ],
            [
                'attribute' => 'amount_utilized_buy',
                'label' => 'Кількість',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($searchModel, $key, $index, $column) use($unitOptions) {
                    return $searchModel['amount_utilized_buy'] . ' ' . ( isset($unitOptions[$searchModel['id_unit']]) ? $unitOptions[$searchModel['id_unit']] : '' );
                }
            ],
            [
                'attribute' => 'id_company',
                'label' => 'Підприємство',
                'content' => function ($model, $key, $index, $column) use($companyOptions) {
                    return isset($companyOptions[$model['id_company']]) ? $companyOptions[$model['id_company']] : '';
                }
            ],
            [
                'attribute' => 'id_utilized_place',
                'label' => 'Місце використання',
                'content' => function ($model, $key, $index, $column) use($places) {
                    if(isset($places[$model['id_utilized_place']])){
                        $place=$places[$model['id_utilized_place']];
                        $path=[];
                        // $path[]=$place;
                        $leaf = $place;
                        $parentId = $place['id_utilized_place_parent'];
                        while( $parentId ){
                            if(isset($places[$parentId])){
                                $place = $places[$parentId];
                                $parentId = $place['id_utilized_place_parent'];
                                $path[]=$place;
                            }else{
                                $parentId=false;
                            }
                        }
                        $html=[];
                        // $path = array_reverse($path);
                        $html[] = "<div><b>{$leaf['name_utilized_place']} {$leaf['address_utilized_place']}</b></div>";
                        foreach($path as $pa){
                            $html[] = "<div>@{$pa['name_utilized_place']} {$pa['address_utilized_place']}</div>";
                        }
                        return join("",$html);
                    }else{
                        return $model['id_utilized_place'];
                    }
                }
            ],

            //'value_kurs_sale',
            // 'date_sale',
            // 'price_usd_sale',
            // 'price_per_unit_uah_sale',
            // 'price_per_unit_usd_sale',
            // 'discount_sale',
            //            [
            //                'attribute' => 'is_paid_sale',
            //                'filterOptions' => ['class' => 'column-id'],
            //                'label' => 'Оплачено',
            //                'content' => function ($searchModel, $key, $index, $column) use($yesNoOptions) {
            //                    return isset($yesNoOptions[$searchModel['is_paid_sale']]) ? $yesNoOptions[$searchModel['is_paid_sale']] : '';
            //                }
            //            ],
//            [
//                'attribute' => 'partPaid',
//                'filterOptions' => ['class' => 'column-id'],
//                'label' => 'Оплачено',
//                'content' => function ($searchModel, $key, $index, $column) use($yesNoOptions) {
//                    return round(100*$searchModel['partPaid'],2)."%";
//                }
//            ],
//            [
//                'attribute' => 'id_invoice', 
//                'label' => 'Накладна №',
//            ],
        ],
    ]);
    ?>
</div>
