<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\jui\DatePicker;
use kartik\widgets\Select2;

$this->title = 'Покупки';
$this->params['breadcrumbs'][] = ['label' => 'Звіти', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $company->name_company, 'url' => ['/company/view', 'id' => $company->id_company]];
$this->params['breadcrumbs'][] = $company->name_company;
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="buy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['buys'],
        'method' => 'get',
    ]); ?>

    <input type="hidden" name="id_company" value="<?=$company->id_company?>">
    
    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->field($searchModel, 'id_invoice') ?>
        </div>

        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_partner')->widget(Select2::classname(), [
                'data' => $partnerOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($searchModel, 'name_product') ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($searchModel, 'id_product')->widget(Select2::classname(), [
                'data' => $productOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;<br>Дата покупки від</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_buy_min',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>
        
        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;<br>Дата покупки до</label>
            <?php
            echo yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date_buy_max',
                'language' => 'uk',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
        </div>

        <div class="col-sm-1">
            <?= $form->field($searchModel, 'id_buy') ?>
        </div>
        <div class="col-sm-1">
            <?php
            echo $form->field($searchModel, 'is_fully_paid_buy')->widget(Select2::classname(), [
                'data' => $yesNoOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-1">
            <?php
            echo $form->field($searchModel, 'is_fully_utilized_buy')->widget(Select2::classname(), [
                'data' => $yesNoOptions,
                'showToggleAll' => false,
                'options' => ['placeholder' => '', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-sm-3">
            <label class="control-label" for="sale-date_sale">&nbsp;<br>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Знайти', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистити', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>


<div class="buy-index">

    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['attribute' => 'id_buy', 'filterOptions' => ['class' => 'column-id'],],
            // 'id_company',
            // 'id_product',
            [
                'attribute' => 'id_partner',
                'label' => 'Партнер',
                'filter' => $partnerOptions,
                'content' => function ($model, $key, $index, $column) use($partnerOptions) {
                    return isset($partnerOptions[$model['id_partner']]) ? $partnerOptions[$model['id_partner']] : '';
                }
            ],
            [
                'attribute' => 'date_buy',
                'label' => 'Дата',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_buy',
                    'language' => 'uk',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.Y', strtotime($model['date_buy']));
                }
            ],
            'name_product',
            [
                'attribute' => 'amount_buy',
                'label' => 'Кількість',
                'filterOptions' => ['class' => 'column-id'],
                // 'filter' => $unitOptions,
                'content' => function ($model, $key, $index, $column) use($unitOptions) {
                    return $model['amount_buy'] . ' ' . ( isset($unitOptions[$model['id_unit']]) ? $unitOptions[$model['id_unit']] : '' );
                }
            ],
            [   'attribute' => 'price_uah_buy', 
                'label' => 'Ціна, грн',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($unitOptions) {
                    return round($model['price_uah_buy'],2);
                }
            ],
            [   'attribute' => 'price_usd_buy', 
                'label' => 'Ціна, USD',
                'filterOptions' => ['class' => 'column-id'],
                'content' => function ($model, $key, $index, $column) use($unitOptions) {
                    return round($model['price_usd_buy'],2);
                }
            ],  
            [
                'attribute' => 'is_fully_paid_buy',
                'label' => 'Оплачено',
                'content' => function ($searchModel, $key, $index, $column) use($yesNoOptions) {
                    return round(100*$searchModel['partPaid'],2)."%";
                }
            ],
            //['attribute' => 'is_fully_paid_buy', 'filterOptions' => ['class' => 'column-id'],],
            [
            'attribute' => 'is_fully_utilized_buy',
            'label' => 'Використано',
            'filter' => $yesNoOptions,
            'content' => function ($model, $key, $index, $column) use($yesNoOptions) {
                return isset($yesNoOptions[$model['is_fully_utilized_buy']]) ? $yesNoOptions[$model['is_fully_utilized_buy']] : '';
            }
        ],
        //['attribute' => 'is_fully_utilized_buy', 'filterOptions' => ['class' => 'column-id'],],
            'id_invoice',
            //  'value_kurs_buy',
            // 'name_product',
            // 'date_buy',
            // 'amount_buy',
            // 'id_unit',
            // 'price_uah_buy',
            // 'price_usd_buy',
            // 'price_per_unit_usd_buy',
            // 'price_per_unit_uah_buy',            
            // 'is_fully_paid_buy',
            // 'is_fully_utilized_buy',
            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
