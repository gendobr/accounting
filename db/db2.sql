/*
SQLyog Community v12.2.6 (32 bit)
MySQL - 5.5.53-0+deb8u1-log : Database - accounting
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `buy` */

DROP TABLE IF EXISTS `buy`;

CREATE TABLE `buy` (
  `id_buy` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID покупки',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `id_product` bigint(20) DEFAULT NULL COMMENT 'Товар',
  `id_partner` bigint(20) DEFAULT NULL COMMENT 'Партнер',
  `value_kurs_buy` double DEFAULT NULL COMMENT 'Курс грн/USD',
  `name_product` varchar(255) DEFAULT NULL COMMENT 'Назва товару',
  `date_buy` date DEFAULT NULL COMMENT 'Дата покупки',
  `amount_buy` double DEFAULT NULL COMMENT 'Кількість товару',
  `id_unit` bigint(20) DEFAULT NULL COMMENT 'Одиниця вимірювання',
  `price_uah_buy` double DEFAULT NULL COMMENT 'Ціна, грн',
  `price_usd_buy` double DEFAULT NULL COMMENT 'Ціна, USD',
  `price_per_unit_usd_buy` double DEFAULT NULL COMMENT 'Ціна за одницю, USD',
  `price_per_unit_uah_buy` double DEFAULT NULL COMMENT 'Ціна за одницю, грн',
  `id_invoice` varchar(100) DEFAULT NULL COMMENT 'Накладна №',
  `is_fully_paid_buy` tinyint(1) DEFAULT '0' COMMENT 'Повністю оплачено',
  `is_fully_utilized_buy` tinyint(1) DEFAULT '0' COMMENT 'Повністю використано',
  PRIMARY KEY (`id_buy`),
  KEY `fkbid_sale` (`id_company`),
  KEY `fkbid_p` (`id_product`),
  KEY `fkbid_pa` (`id_partner`),
  KEY `id_unit` (`id_unit`),
  CONSTRAINT `buy_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`),
  CONSTRAINT `buy_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`),
  CONSTRAINT `buy_ibfk_3` FOREIGN KEY (`id_partner`) REFERENCES `partner` (`id_partner`),
  CONSTRAINT `buy_ibfk_4` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `buy` */

LOCK TABLES `buy` WRITE;

insert  into `buy`(`id_buy`,`id_company`,`id_product`,`id_partner`,`value_kurs_buy`,`name_product`,`date_buy`,`amount_buy`,`id_unit`,`price_uah_buy`,`price_usd_buy`,`price_per_unit_usd_buy`,`price_per_unit_uah_buy`,`id_invoice`,`is_fully_paid_buy`,`is_fully_utilized_buy`) values (2,1,2,1,10,'Test product 1','2016-12-29',1,3,230,23,23,230,'665765',0,0);

UNLOCK TABLES;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id_category` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID категорії',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `name_category` varchar(100) DEFAULT NULL COMMENT 'Назва категорії',
  PRIMARY KEY (`id_category`),
  KEY `fkcid_c` (`id_company`),
  CONSTRAINT `category_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

LOCK TABLES `category` WRITE;

insert  into `category`(`id_category`,`id_company`,`name_category`) values (1,2,'category1');
insert  into `category`(`id_category`,`id_company`,`name_category`) values (2,2,'Категорія1');
insert  into `category`(`id_category`,`id_company`,`name_category`) values (3,1,'Cat1');
insert  into `category`(`id_category`,`id_company`,`name_category`) values (4,1,'Cat2');

UNLOCK TABLES;

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id_company` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID підприємства',
  `name_company` varchar(255) DEFAULT NULL COMMENT 'Назва підприємства',
  `kontakt_company` varchar(255) DEFAULT NULL COMMENT 'Контакти підприємства',
  `note_company` varchar(255) DEFAULT NULL COMMENT 'Примітки',
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `company` */

LOCK TABLES `company` WRITE;

insert  into `company`(`id_company`,`name_company`,`kontakt_company`,`note_company`) values (1,'Company1','vfd','c');
insert  into `company`(`id_company`,`name_company`,`kontakt_company`,`note_company`) values (2,'Компанія','ваааа','ипаа');

UNLOCK TABLES;

/*Table structure for table `income_sale` */

DROP TABLE IF EXISTS `income_sale`;

CREATE TABLE `income_sale` (
  `id_income_sale` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID частини оплати',
  `id_sale` bigint(20) DEFAULT NULL COMMENT 'Продаж',
  `id_payment` bigint(20) DEFAULT NULL COMMENT 'Платіж',
  `value_kurs_income_sale` double DEFAULT NULL COMMENT 'Курс',
  `date_income_sale` date DEFAULT NULL COMMENT 'Дата надходження',
  `amount_uah_income_sale` double DEFAULT NULL COMMENT 'Величина, грн',
  `amount_usd_income_sale` double DEFAULT NULL COMMENT 'Величина, USD',
  PRIMARY KEY (`id_income_sale`),
  KEY `fkisid_sale` (`id_sale`),
  KEY `fkisid_pa` (`id_payment`),
  CONSTRAINT `income_sale_ibfk_1` FOREIGN KEY (`id_sale`) REFERENCES `sale` (`id_sale`),
  CONSTRAINT `income_sale_ibfk_2` FOREIGN KEY (`id_payment`) REFERENCES `payment` (`id_payment`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `income_sale` */

LOCK TABLES `income_sale` WRITE;

insert  into `income_sale`(`id_income_sale`,`id_sale`,`id_payment`,`value_kurs_income_sale`,`date_income_sale`,`amount_uah_income_sale`,`amount_usd_income_sale`) values (8,3,1,10,'2016-12-29',500,50);
insert  into `income_sale`(`id_income_sale`,`id_sale`,`id_payment`,`value_kurs_income_sale`,`date_income_sale`,`amount_uah_income_sale`,`amount_usd_income_sale`) values (9,4,1,10,'2016-12-29',100,10);

UNLOCK TABLES;

/*Table structure for table `kurs` */

DROP TABLE IF EXISTS `kurs`;

CREATE TABLE `kurs` (
  `id_kurs` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID курсу',
  `date_kurs` date DEFAULT NULL COMMENT 'Дата курсу',
  `value_kurs` double DEFAULT NULL COMMENT 'Значення курсу',
  PRIMARY KEY (`id_kurs`),
  UNIQUE KEY `date_kurs` (`date_kurs`,`value_kurs`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `kurs` */

LOCK TABLES `kurs` WRITE;

insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (2,'2016-12-17',23);
insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (1,'2016-12-24',22);
insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (3,'2016-12-25',25);
insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (4,'2016-12-27',23);
insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (5,'2016-12-28',24);
insert  into `kurs`(`id_kurs`,`date_kurs`,`value_kurs`) values (6,'2016-12-29',10);

UNLOCK TABLES;

/*Table structure for table `partner` */

DROP TABLE IF EXISTS `partner`;

CREATE TABLE `partner` (
  `id_partner` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID партнера',
  `name_partner` varchar(255) DEFAULT NULL COMMENT 'Назва або ім''я',
  `kontakt_partner` varchar(255) DEFAULT NULL COMMENT 'Контактна інформація',
  `note_partner` varchar(255) DEFAULT NULL COMMENT 'Примітки',
  `id_company` bigint(20) NOT NULL,
  PRIMARY KEY (`id_partner`),
  KEY `id_company` (`id_company`),
  CONSTRAINT `partner_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `partner` */

LOCK TABLES `partner` WRITE;

insert  into `partner`(`id_partner`,`name_partner`,`kontakt_partner`,`note_partner`,`id_company`) values (1,'Партнер 1','Партнер 1 контакти','Партнер 1 примітки',1);

UNLOCK TABLES;

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `id_payment` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID платежу',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `id_partner` bigint(20) DEFAULT NULL COMMENT 'Партнер',
  `date_payment` date DEFAULT NULL COMMENT 'Дата платежу',
  `value_kurs_payment` double DEFAULT NULL COMMENT 'Значення курсу',
  `amount_uah_payment` double DEFAULT NULL COMMENT 'Величина, ГРН',
  `amount_usd_payment` double DEFAULT NULL COMMENT 'Величина, USD',
  `type_payment` enum('income','outcome') DEFAULT NULL COMMENT 'Тип платежу',
  `is_used` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_payment`),
  KEY `fkpsid_c` (`id_company`),
  KEY `fkpsid_pa` (`id_partner`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`),
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`id_partner`) REFERENCES `partner` (`id_partner`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `payment` */

LOCK TABLES `payment` WRITE;

insert  into `payment`(`id_payment`,`id_company`,`id_partner`,`date_payment`,`value_kurs_payment`,`amount_uah_payment`,`amount_usd_payment`,`type_payment`,`is_used`) values (1,1,1,'2016-12-29',10,600,60,'income',1);
insert  into `payment`(`id_payment`,`id_company`,`id_partner`,`date_payment`,`value_kurs_payment`,`amount_uah_payment`,`amount_usd_payment`,`type_payment`,`is_used`) values (2,1,1,'2016-12-29',10,200,20,'outcome',1);

UNLOCK TABLES;

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id_product` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID товару',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `id_category` bigint(20) DEFAULT NULL COMMENT 'Категорія',
  `id_unit` bigint(10) DEFAULT NULL COMMENT 'Одиниці вимірювання',
  `price_per_unit_usd_product` double DEFAULT NULL COMMENT 'Ціна за одиницю, USD',
  `name_product` varchar(255) DEFAULT NULL COMMENT 'Назва товару',
  `note_product` varchar(255) DEFAULT NULL COMMENT 'Примітки',
  `is_visible_product` tinyint(1) DEFAULT '1' COMMENT 'Видимість',
  PRIMARY KEY (`id_product`),
  KEY `fkpid_c` (`id_company`),
  KEY `fkpid_ca` (`id_category`),
  KEY `id_unit` (`id_unit`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`),
  CONSTRAINT `product_ibfk_3` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `product` */

LOCK TABLES `product` WRITE;

insert  into `product`(`id_product`,`id_company`,`id_category`,`id_unit`,`price_per_unit_usd_product`,`name_product`,`note_product`,`is_visible_product`) values (1,2,1,1,111,'1111.1','11',1);
insert  into `product`(`id_product`,`id_company`,`id_category`,`id_unit`,`price_per_unit_usd_product`,`name_product`,`note_product`,`is_visible_product`) values (2,1,3,3,111,'Test product 1','qqq',1);

UNLOCK TABLES;

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id_role` bigint(20) NOT NULL AUTO_INCREMENT,
  `code_role` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `name_role` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `id_company` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_role`),
  KEY `id_company` (`id_company`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `role` */

LOCK TABLES `role` WRITE;

insert  into `role`(`id_role`,`code_role`,`name_role`,`id_company`) values (1,'admin','Адміністратор',NULL);
insert  into `role`(`id_role`,`code_role`,`name_role`,`id_company`) values (2,'booker','Бухгалтер',NULL);
insert  into `role`(`id_role`,`code_role`,`name_role`,`id_company`) values (3,'operator','Оператор',1);
insert  into `role`(`id_role`,`code_role`,`name_role`,`id_company`) values (4,'operator','Оператор',2);

UNLOCK TABLES;

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id_role` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_role`,`id_user`),
  KEY `id_user` (`id_user`),
  KEY `pky` (`id_role`),
  CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `role_user` */

LOCK TABLES `role_user` WRITE;

insert  into `role_user`(`id_role`,`id_user`) values (1,1);
insert  into `role_user`(`id_role`,`id_user`) values (3,2);

UNLOCK TABLES;

/*Table structure for table `sale` */

DROP TABLE IF EXISTS `sale`;

CREATE TABLE `sale` (
  `id_sale` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID продажу',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `id_product` bigint(20) DEFAULT NULL COMMENT 'Товар',
  `id_partner` bigint(20) DEFAULT NULL COMMENT 'Партнер',
  `id_unit` bigint(20) NOT NULL COMMENT 'Одиниці вимірювання',
  `amount` double DEFAULT NULL COMMENT 'Кількість одниць товару',
  `value_kurs_sale` double DEFAULT NULL COMMENT 'Курс грн/USD',
  `date_sale` date DEFAULT NULL COMMENT 'Дата продажу',
  `name_product` varchar(255) DEFAULT NULL COMMENT 'Назва товару',
  `price_uah_sale` double DEFAULT NULL COMMENT 'Ціна, грн',
  `price_usd_sale` double DEFAULT NULL COMMENT 'Ціна, USD',
  `price_per_unit_uah_sale` double DEFAULT NULL COMMENT 'Ціна за одиницю, ГРН',
  `price_per_unit_usd_sale` double DEFAULT NULL COMMENT 'Ціна за одиницю, USD',
  `discount_sale` double DEFAULT NULL COMMENT 'Знижка',
  `is_paid_sale` tinyint(1) DEFAULT '0' COMMENT 'повністю оплачено',
  `id_invoice` varchar(100) DEFAULT NULL COMMENT 'Накладна №',
  PRIMARY KEY (`id_sale`),
  KEY `fksid_c` (`id_company`),
  KEY `fksid_p` (`id_product`),
  KEY `fksid_pa` (`id_partner`),
  KEY `sale_ibfk_4` (`id_unit`),
  CONSTRAINT `sale_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`),
  CONSTRAINT `sale_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`),
  CONSTRAINT `sale_ibfk_3` FOREIGN KEY (`id_partner`) REFERENCES `partner` (`id_partner`),
  CONSTRAINT `sale_ibfk_4` FOREIGN KEY (`id_unit`) REFERENCES `unit` (`id_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sale` */

LOCK TABLES `sale` WRITE;

insert  into `sale`(`id_sale`,`id_company`,`id_product`,`id_partner`,`id_unit`,`amount`,`value_kurs_sale`,`date_sale`,`name_product`,`price_uah_sale`,`price_usd_sale`,`price_per_unit_uah_sale`,`price_per_unit_usd_sale`,`discount_sale`,`is_paid_sale`,`id_invoice`) values (3,1,2,1,3,3,10,'2016-12-29','Test product 1',500,50,166.667,16.6667,0,1,'12344');
insert  into `sale`(`id_sale`,`id_company`,`id_product`,`id_partner`,`id_unit`,`amount`,`value_kurs_sale`,`date_sale`,`name_product`,`price_uah_sale`,`price_usd_sale`,`price_per_unit_uah_sale`,`price_per_unit_usd_sale`,`discount_sale`,`is_paid_sale`,`id_invoice`) values (4,1,2,1,3,1,10,'2016-12-29','Test product 1',200,20,200,20,0,0,'22222');

UNLOCK TABLES;

/*Table structure for table `spend_buy` */

DROP TABLE IF EXISTS `spend_buy`;

CREATE TABLE `spend_buy` (
  `id_spend_buy` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID виплати',
  `id_payment` bigint(20) DEFAULT NULL COMMENT 'ID платежу',
  `id_buy` bigint(20) DEFAULT NULL COMMENT 'ID покупки',
  `amount_grn_spend_buy` double DEFAULT NULL COMMENT 'Величина виплати, ГРН',
  `amount_usd_spend_buy` double DEFAULT NULL COMMENT 'Величина виплати, USD',
  PRIMARY KEY (`id_spend_buy`),
  KEY `fksbid_sale` (`id_buy`),
  KEY `fksbid_p` (`id_payment`),
  CONSTRAINT `spend_buy_ibfk_1` FOREIGN KEY (`id_buy`) REFERENCES `buy` (`id_buy`),
  CONSTRAINT `spend_buy_ibfk_2` FOREIGN KEY (`id_payment`) REFERENCES `payment` (`id_payment`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `spend_buy` */

LOCK TABLES `spend_buy` WRITE;

insert  into `spend_buy`(`id_spend_buy`,`id_payment`,`id_buy`,`amount_grn_spend_buy`,`amount_usd_spend_buy`) values (6,2,2,200,20);

UNLOCK TABLES;

/*Table structure for table `unit` */

DROP TABLE IF EXISTS `unit`;

CREATE TABLE `unit` (
  `id_unit` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID одиниці вимірювання',
  `id_company` bigint(20) NOT NULL COMMENT 'Підприємство',
  `name_unit` varchar(32) DEFAULT NULL COMMENT 'Назва одиниці вимірювання',
  PRIMARY KEY (`id_unit`),
  KEY `id_company` (`id_company`),
  CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `unit` */

LOCK TABLES `unit` WRITE;

insert  into `unit`(`id_unit`,`id_company`,`name_unit`) values (1,2,'кг');
insert  into `unit`(`id_unit`,`id_company`,`name_unit`) values (2,2,'шт');
insert  into `unit`(`id_unit`,`id_company`,`name_unit`) values (3,1,'шт');

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID користувача',
  `lastname_user` varchar(100) DEFAULT NULL COMMENT 'Прізвище',
  `firstname_user` varchar(100) DEFAULT NULL COMMENT 'Ім''я',
  `surname_user` varchar(100) DEFAULT NULL COMMENT 'По батькові',
  `login_user` varchar(100) DEFAULT NULL,
  `password_user` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL COMMENT 'Електропошта',
  `note_user` varchar(255) DEFAULT NULL COMMENT 'Примітки',
  `access_token` varchar(100) DEFAULT NULL,
  `auth_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id_user`,`lastname_user`,`firstname_user`,`surname_user`,`login_user`,`password_user`,`email_user`,`note_user`,`access_token`,`auth_key`) values (1,'admin','admin','admin','admin','fj8cEnUB8jnqI','webmaster@znu.in.ua',NULL,NULL,NULL);
insert  into `user`(`id_user`,`lastname_user`,`firstname_user`,`surname_user`,`login_user`,`password_user`,`email_user`,`note_user`,`access_token`,`auth_key`) values (2,'Operator','Oper','Operovich','operator','fj/Nf0lOvgOK6','operCom1@email.ru','',NULL,'GJtPRtiVJremePYLUg2Lvx-vrr87M3Gv');

UNLOCK TABLES;

/*Table structure for table `utilized_buy` */

DROP TABLE IF EXISTS `utilized_buy`;

CREATE TABLE `utilized_buy` (
  `id_utilized_buy` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID використання',
  `id_buy` bigint(20) DEFAULT NULL COMMENT 'Дата покупки',
  `id_utilized_place` bigint(20) DEFAULT NULL COMMENT 'Місце використання',
  `date_utilized_buy` date DEFAULT NULL COMMENT 'Дата використання',
  `amount_utilized_buy` double DEFAULT NULL COMMENT 'Використана кількість',
  PRIMARY KEY (`id_utilized_buy`),
  KEY `fkubid_sale` (`id_buy`),
  KEY `fkubid_sb` (`id_utilized_place`),
  CONSTRAINT `utilized_buy_ibfk_1` FOREIGN KEY (`id_buy`) REFERENCES `buy` (`id_buy`),
  CONSTRAINT `utilized_buy_ibfk_2` FOREIGN KEY (`id_utilized_place`) REFERENCES `utilized_place` (`id_utilized_place`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `utilized_buy` */

LOCK TABLES `utilized_buy` WRITE;

UNLOCK TABLES;

/*Table structure for table `utilized_place` */

DROP TABLE IF EXISTS `utilized_place`;

CREATE TABLE `utilized_place` (
  `id_utilized_place` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID місця використання',
  `id_company` bigint(20) DEFAULT NULL COMMENT 'Підприємство',
  `name_utilized_place` varchar(255) DEFAULT NULL COMMENT 'Назва місця використання',
  `address_utilized_place` varchar(255) DEFAULT NULL COMMENT 'Адреса місця використання',
  `notes_utilized_place` varchar(255) DEFAULT NULL COMMENT 'Примітки',
  `id_utilized_place_parent` bigint(20) DEFAULT NULL COMMENT 'Є частиною',
  `is_visible_utilized_place` tinyint(1) DEFAULT NULL COMMENT 'Видиме місце використання',
  PRIMARY KEY (`id_utilized_place`),
  KEY `fkosid_company` (`id_company`),
  CONSTRAINT `object_sale_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `utilized_place` */

LOCK TABLES `utilized_place` WRITE;

insert  into `utilized_place`(`id_utilized_place`,`id_company`,`name_utilized_place`,`address_utilized_place`,`notes_utilized_place`,`id_utilized_place_parent`,`is_visible_utilized_place`) values (1,1,'place1','place1','place1',NULL,1);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
