<?php

namespace app\controllers;

use Yii;
use app\models\SpendBuy;
use app\models\SpendBuySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpendBuyController implements the CRUD actions for SpendBuy model.
 */
class SpendBuyController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpendBuy models.
     * @return mixed
     */
    public function actionIndex() {

        if (Yii::$app->user->identity->is("admin|booker")) {
            
            $searchModel = new SpendBuySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);            

        } elseif (!Yii::$app->user->isGuest) {
            
            $searchModel = new SpendBuySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
            $company_ids = Yii::$app->user->identity->getCompanyIds();
            if (count($company_ids) == 0) {
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
            $dataProvider->query->andFilterWhere(['in', 'buy.id_company', $company_ids]);
        } else {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }



        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SpendBuy model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new SpendBuy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $post = Yii::$app->request->post();
        $id_buy = $post['SpendBuy']['id_buy'];
        $buy = Sale::findOne($id_buy);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$buy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new SpendBuy();
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_spend_buy]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SpendBuy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_spend_buy]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SpendBuy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpendBuy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpendBuy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SpendBuy::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
