<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Company;
use app\models\Report;
use app\models\Kurs;
//use app\models\SaleSearch;
//use app\models\BuySearch;
//use yii\web\NotFoundHttpException;
//use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Description of ReportController
 *
 * @author dobro
 */
class ReportController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
        ];
    }

    public function actionIndex() {
        $model = new Report();
        if (Yii::$app->user->identity->is("admin|booker")) {
            $companyOptions = $model->getCompanyOptions(false);
        }elseif(!Yii::$app->user->isGuest){
            $company_ids = Yii::$app->user->identity->getCompanyIds();
            if (count($company_ids) == 0) {
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
            $companyOptions = $model->getCompanyOptions($company_ids);
        }else{
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('index', [
                    'companyOptions' => $companyOptions,
        ]);
    }

    public function actionSales($id_company) {

        if (!Yii::$app->user->identity->is("admin|booker|operator=$id_company")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new Report();
        $report = $model->saleReport(Yii::$app->request->queryParams);
        $report['query']->andFilterWhere([ 'id_company' => $id_company,]);

        $company = Company::findOne($id_company);
        $partnerOptions = ArrayHelper::map($company->getPartners()->all(), 'id_partner', 'name_partner');
        $unitOptions = ArrayHelper::map($company->getUnits()->all(), 'id_unit', 'name_unit');
        $productOptions = ArrayHelper::map($company->getProducts()->all(), 'id_product', 'name_product');
        $yesNoOptions = [0 => 'Ні', 1 => 'Так'];

        return $this->render('sales', [
                    'searchModel' => $report['model'],
                    'dataProvider' => $report['dataProvider'],
                    'company' => $company,
                    'partnerOptions' => $partnerOptions,
                    'unitOptions' => $unitOptions,
                    'productOptions' => $productOptions,
                    'yesNoOptions' => $yesNoOptions
        ]);
    }

    public function actionBuys($id_company) {

        if (!Yii::$app->user->identity->is("admin|booker|operator=$id_company")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new Report();
        $report = $model->buyReport(Yii::$app->request->queryParams);
        $report['query']->andFilterWhere([ 'id_company' => $id_company,]);

        $company = Company::findOne($id_company);
        $partnerOptions = ArrayHelper::map($company->getPartners()->all(), 'id_partner', 'name_partner');
        $unitOptions = ArrayHelper::map($company->getUnits()->all(), 'id_unit', 'name_unit');
        $productOptions = ArrayHelper::map($company->getProducts()->all(), 'id_product', 'name_product');
        $yesNoOptions = [0 => 'Ні', 1 => 'Так'];
        return $this->render('buys', [
                    'searchModel' => $report['model'],
                    'dataProvider' => $report['dataProvider'],
                    'company' => $company,
                    'partnerOptions' => $partnerOptions,
                    'unitOptions' => $unitOptions,
                    'productOptions' => $productOptions,
                    'yesNoOptions' => $yesNoOptions
        ]);
    }

    public function actionPayments() {
        
        if (Yii::$app->user->identity->is("admin|booker")) {
            $company_ids = false;
        }elseif(!Yii::$app->user->isGuest){
            $company_ids = Yii::$app->user->identity->getCompanyIds();
            if (count($company_ids) == 0) {
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
        }else{
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new Report();

        $report = $model->paymentReport(Yii::$app->request->queryParams, $company_ids);

        $yesNoOptions = [0 => 'Ні', 1 => 'Так'];
        $typePaymentOptions = ['income' => 'Вхідний', 'outcome' => 'Вихідний'];

        return $this->render('payments', [
                    'searchModel' => $report['model'],
                    'dataProvider' => $report['dataProvider'],
                    'companyOptions' => $model->getCompanyOptions($company_ids),
                    'partnerOptions' => $model->getPartnerOptions($company_ids),
                    'yesNoOptions' => $yesNoOptions,
                    'typePaymentOptions' => $typePaymentOptions
        ]);
    }

    public function actionBalance() {
        
        if (Yii::$app->user->identity->is("admin|booker")) {
            $company_ids = false;
        }elseif(!Yii::$app->user->isGuest){
            $company_ids = Yii::$app->user->identity->getCompanyIds();
            if (count($company_ids) == 0) {
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
        }else{
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        
        $model = new Report();

        $report = $model->balanceReport(Yii::$app->request->queryParams, $company_ids);

        $kurs = Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();

        return $this->render('balance', [
                    'searchModel' => $report['model'],
                    'dataProvider' => $report['dataProvider'],
                    'companyOptions' => $model->getCompanyOptions($company_ids),
                    'partnerOptions' => $model->getPartnerOptions($company_ids),
                    'kurs' => $kurs
        ]);
    }

    public function actionUsage() {
        if (Yii::$app->user->identity->is("admin|booker")) {
            $company_ids = false;
        }elseif(!Yii::$app->user->isGuest){
            $company_ids = Yii::$app->user->identity->getCompanyIds();
            if (count($company_ids) == 0) {
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
        }else{
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $model = new Report();
        $report = $model->usageReport(Yii::$app->request->queryParams);
        return $this->render('usage', [
                    'searchModel' => $report['model'],
                    'dataProvider' => $report['dataProvider'],
                    'companyOptions' => $model->getCompanyOptions($company_ids),
                    'partnerOptions' => $model->getPartnerOptions($company_ids),
                    'unitOptions' => $model->getUnitOptions($company_ids),
                    'places' => $report['places']
        ]);
    }

}
