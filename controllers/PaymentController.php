<?php

namespace app\controllers;

use Yii;
use app\models\Payment;
use app\models\PaymentSearch;
use app\models\Company;
use app\models\Buy;
use app\models\Sale;
use app\models\Kurs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex($id_company) {
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_company' => $id_company,]);
        $company = Company::findOne($id_company);
        $partnerOptions = ArrayHelper::map($company->getPartners()->all(), 'id_partner', 'name_partner');

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'partnerOptions' => $partnerOptions,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    public function actionBuy($id) {

        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        // get list of unpaid buys
        $buy = new Buy();
        $unpaidBuys = $buy->getAvailableBuys($model->id_company, $model->id_partner);

        return $this->render('buy', [
                    'model' => $model,
                    'unpaidBuys' => $unpaidBuys
        ]);
    }

    public function actionSpendbuy($id_payment, $id_buy) {
        
        
        $buy = Buy::findOne($id_buy);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$buy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        
        
        $payment = new Payment();
        $payment->usePayment($id_buy, $id_payment);
        return $this->redirect(['buy', 'id' => $id_payment]);
    }

    public function actionSale($id) {

        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        // get list of unpaid sales
        $sale = new Sale();
        $unpaidSales = $sale->getAvailableSales($model->id_company, $model->id_partner);

        return $this->render('sale', [
                    'model' => $model,
                    'unpaidSales' => $unpaidSales
        ]);
    }

    public function actionIncomesale($id_payment, $id_sale) {

        $sale = Sale::findOne($id_sale);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$sale->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }


        $payment = new Payment();
        $payment->usePayment($id_sale, $id_payment);
        return $this->redirect(['sale', 'id' => $id_payment]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_company) {

        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new Payment();
        $model->id_company = $id_company;
        if ($model->load(Yii::$app->request->post())) {
            $model->autoFill();
            if ($model->save()) {
                // $model->applyPayment();
                return $this->redirect(['view', 'id' => $model->id_payment]);
            }
        }
        $company = Company::findOne($id_company);
        return $this->render('create', [
                    'model' => $model,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $oldModel = $model->attributes;
        // var_dump($oldModel);exit();
        if ($model->load(Yii::$app->request->post())) {

            $dateSaleKurs = Kurs::find()->where(['=', 'date_kurs', $model->date_payment])->one();
            if (!$dateSaleKurs) {
                \Yii::$app->getSession()->setFlash('error', "Курс на дату {$model->date_payment} не знайдено");
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
            $model->autoFill();

            if ($model->save()) {
                $model->clearPayment();
                return $this->redirect(['view', 'id' => $model->id_payment]);
            }
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model->clearPayment();
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
