<?php

namespace app\controllers;

use Yii;
use app\models\Sale;
use app\models\Payment;
use app\models\Kurs;
use app\models\SaleSearch;
use yii\web\Controller;
use app\models\Company;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SaleController implements the CRUD actions for Sale model.
 */
class SaleController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sale models.
     * @return mixed
     */
    public function actionIndex($id_company) {

        if (!Yii::$app->user->identity->is("admin|booker|operator=$id_company")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $searchModel = new SaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_company' => $id_company,]);
        $company = Company::findOne($id_company);
        $partnerOptions = ArrayHelper::map($company->getPartners()->all(), 'id_partner', 'name_partner');
        $unitOptions = ArrayHelper::map($company->getUnits()->all(), 'id_unit', 'name_unit');
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'partnerOptions' => $partnerOptions,
                    'unitOptions' => $unitOptions,
        ]);
    }

    public function actionPayment($id) {

        $model = $this->findModel($id);

        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }


        $kurs = Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();
        //echo date('Y-m-d');
        //var_dump($kurs);
        // exit();
        $payment = new Payment();
        $availablePayments = $payment->getAvailablePayments($model->id_company, $model->id_partner);
        return $this->render('payment', [
                    'model' => $model,
                    'availablePayments' => $availablePayments,
                    'kurs' => $kurs
        ]);
    }

    public function actionUsepayment($id_sale, $id_payment) {

        $sale = Sale::findOne($id_sale);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$sale->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $payment = new Payment();
        $payment->usePayment($id_sale, $id_payment);
        return $this->redirect(['payment', 'id' => $id_sale]);
    }

    /**
     * Displays a single Sale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('view', [
                    'model' => $model
        ]);
    }

    /**
     * Creates a new Sale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_company) {
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $model = new Sale();
        $model->id_company = $id_company;
        if ($model->load(Yii::$app->request->post())) {
            // fill-in hidden fields
            $model->autoPostFill();
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_sale]);
            }
        }
        $company = Company::findOne($id_company);
        return $this->render('create', [
                    'model' => $model,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing Sale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->autoPostFill();
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id_sale]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $id_company = $model->id_company;
        $model->delete();
        return $this->redirect(['index', 'id_company' => $id_company]);
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
