<?php

namespace app\controllers;

use Yii;
use app\models\UtilizedPlace;
use app\models\UtilizedPlaceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Company;
/**
 * UtilizedPlaceController implements the CRUD actions for UtilizedPlace model.
 */
class UtilizedPlaceController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UtilizedPlace models.
     * @return mixed
     */
    public function actionIndex($id_company) {
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        
        $searchModel = new UtilizedPlaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_company' => $id_company,]);
        $company = Company::findOne($id_company);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company, 
        ]);
    }

    /**
     * Displays a single UtilizedPlace model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model= $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new UtilizedPlace model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_company) {
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        
        $model = new UtilizedPlace();
        $model->id_company = $id_company;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_utilized_place]);
        }

        $company = Company::findOne($id_company);

        return $this->render('create', [
                    'model' => $model,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing UtilizedPlace model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_utilized_place]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UtilizedPlace model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $modle->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UtilizedPlace model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UtilizedPlace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UtilizedPlace::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
