<?php

namespace app\controllers;

use Yii;
use app\models\Partner;
use app\models\PartnerSearch;
use app\models\Kurs;
use yii\web\Controller;
use app\models\Company;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PartnerController implements the CRUD actions for Partner model.
 */
class PartnerController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex($id_company) {
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        
        $searchModel = new PartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_company' => $id_company,]);
        $company = Company::findOne($id_company);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
        ]);
    }

    /**
     * Displays a single Partner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        
        // show balance
        $buys = $model->getBuys()->andWhere(['not', [ 'is_fully_paid_buy' => 1]])->all();
        $buyBalance = 0;
        foreach ($buys as $buy) {
            $amount = $buy->price_usd_buy;
            $spendBuys = $buy->getSpendBuys()->all();
            foreach ($spendBuys as $sb) {
                $amount-=$sb->amount_usd_spend_buy;
            }
            if ($amount > 0) {
                $buyBalance+=$amount;
            }
        }

        $sales = $model->getSales()->andWhere(['not', [ 'is_paid_sale' => 1]])->all();
        $saleBalance = 0;
        foreach ($sales as $sale) {
            $amount = $sale->price_usd_sale;
            $incomeSales = $sale->getIncomeSales()->all();
            foreach ($incomeSales as $is) {
                $amount -= $is->amount_usd_income_sale;
            }
            if ($amount > 0) {
                $saleBalance+=$amount;
            }
        }

        $payments = $model->getPayments()->andWhere(['not', [ 'is_used' => 1]])->all();
        $paymentBalance = ['income' => 0, 'outcome' => 0];
        foreach ($payments as $payment) {

            switch ($payment->type_payment) {
                case 'income':
                    $amount = $payment->amount_usd_payment;
                    $incomeSales = $sale->getIncomeSales()->all();
                    foreach ($incomeSales as $is) {
                        $amount -= $is->amount_usd_income_sale;
                    }
                    if ($amount > 0) {
                        $paymentBalance['income']+=$amount;
                    }
                    break;
                case 'outcome':
                    $amount = $payment->amount_usd_payment;
                    $spendBuys = $payment->getSpendBuys()->all();
                    foreach ($spendBuys as $sb) {
                        $amount-=$sb->amount_usd_spend_buy;
                    }
                    if ($amount > 0) {
                        $paymentBalance['outcome']+=$amount;
                    }
                    break;
            }
        }


        $kurs = Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();
        
        return $this->render('view', [
                    'model' => $model,
                    'buyBalance' => 'buyBalance',
                    'saleBalance' => $saleBalance,
                    'paymentBalance' => $paymentBalance,
                    'kurs'=>$kurs
        ]);
    }

    /**
     * Creates a new Partner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_company) {
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $model = new Partner();
        $model->id_company = $id_company;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_partner]);
        }
        $company = Company::findOne($id_company);
        return $this->render('create', [
                    'model' => $model,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing Partner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_partner]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Partner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Partner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
