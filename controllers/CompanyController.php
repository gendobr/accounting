<?php

namespace app\controllers;

use Yii;
use app\models\Company;
use app\models\Role;
use app\models\CompanySaleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // restrict id_company if user is not admin
        if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        if (!Yii::$app->user->identity->is("admin|booker")) {
            $company_ids=Yii::$app->user->identity->getCompanyIds();
            if(count($company_ids)==0){
                throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
            }
            // var_dump($company_ids);
            $dataProvider->query->andFilterWhere(['in', 'id_company', $company_ids]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();

        if (!Yii::$app->user->identity->is("admin|booker")) {
           throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $role = new Role();
            $role->code_role='operator';
            $role->name_role="Оператор";
            $role->id_company = $model->id_company;
            $role->save();
            
            return $this->redirect(['view', 'id' => $model->id_company]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_company]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker")) {
           throw new ForbiddenHttpException('Only admin can see this page.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
