<?php

namespace app\controllers;

use Yii;
use app\models\Kurs;
use app\models\KursSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KursController implements the CRUD actions for Kurs model.
 */
class KursController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kurs models.
     * @return mixed
     */
    public function actionIndex() {
        if (!Yii::$app->user->identity->is('admin|booker')) {
            throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        $searchModel = new KursSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kurs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!Yii::$app->user->identity->is('admin|booker')) {
            throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kurs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!Yii::$app->user->identity->is('admin|booker')) {
            throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        $model = new Kurs();

        // check if today kurs is set
        $todayKurs = Kurs::find()->where(['=', 'date_kurs', date('Y-m-d')])->one();
        if ($todayKurs !== null) {
            return $this->redirect(['update', 'id' => $todayKurs->id_kurs]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kurs]);
        } else {
            $model->date_kurs = date('Y-m-d');
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kurs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!Yii::$app->user->identity->is('admin|booker')) {
            throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kurs]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kurs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!Yii::$app->user->identity->is('admin|booker')) {
            throw new ForbiddenHttpException('Only admin or booker can see this page.');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kurs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kurs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Kurs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
