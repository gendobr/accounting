<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        if (!Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Only admin can see this page.');
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Only admin can see this page.');
        }
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Only admin can see this page.');
        }
        $model = new User();
        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ((strlen($post["upw1"]) > 0) && ($post["upw1"] == $post["upw2"])) {
                $model->setPassword($post["upw1"]);
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Only admin can see this page.');
        }
        $model = $this->findModel($id);


        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            if ((strlen($post["upw1"]) > 0) && ($post["upw1"] == $post["upw2"])) {
                $model->setPassword($post["upw1"]);
                $model->save();
            }
            // upw1
            // upw2
            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Only admin can see this page.');
        }
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionChangePassword($id) {

        $model = $this->findModel($id);

        if (Yii::$app->user->identity->id != $model->id) {
            throw new ForbiddenHttpException('Only owner can see this page.');
        }
        $post = Yii::$app->request->post();
        unset($post['User']['roles']);

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

}
