<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use app\models\Company;
use app\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex($id_company) {
        
        if (!Yii::$app->user->identity->is("admin|booker|operator=$id_company")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_company' => $id_company,]);
        $company = Company::findOne($id_company);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_company) {
        if (!Yii::$app->user->identity->is("admin|booker|operator={$id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $model = new Product();
        $model->id_company = $id_company;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_product]);
        }
        $company = Company::findOne($id_company);
        return $this->render('create', [
                    'model' => $model,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_product]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->id_company}")) {
           throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
