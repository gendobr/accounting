<?php

namespace app\controllers;

use Yii;
use app\models\UtilizedBuy;
use app\models\Buy;
use app\models\Company;
use app\models\UtilizedBuySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UtilizedBuyController implements the CRUD actions for UtilizedBuy model.
 */
class UtilizedBuyController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UtilizedBuy models.
     * @return mixed
     */
    public function actionIndex($id_buy) {
        $buy = Buy::findOne($id_buy);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$buy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $company = $buy->idCompany;

        $searchModel = new UtilizedBuySearch();
        $searchModel->id_buy = $buy->id_buy;
        $searchModel->id_company = $buy->id_company;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_buy' => $buy->id_buy,]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'buy' => $buy,
                    'company' => $company,
        ]);
    }

    /**
     * Displays a single UtilizedBuy model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model=$this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }
        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new UtilizedBuy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_buy) {
        $model = new UtilizedBuy();
        $buy = Buy::findOne($id_buy);
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$buy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $company = $buy->idCompany;
        $model->id_buy = $id_buy;
        $errorMessage = '';
        if ($model->load(Yii::$app->request->post())) {
            $remainder = $buy->getRemainder();
            if (($remainder - $model->amount_utilized_buy) < 0) {
                $name_unit=$buy->idUnit->name_unit;
                $errorMessage.="Залишок {$remainder} {$name_unit}. Неможливо використати {$model->amount_utilized_buy} {$name_unit}.";
            }
            if (strlen($errorMessage) > 0) {
                \Yii::$app->getSession()->setFlash('error', $errorMessage);
            }

            if (strlen($errorMessage) == 0 && $model->save()) {
                $model->setUtilizedBuy();
                //return $this->redirect(['view', 'id' => $model->id_utilized_buy]);
                return $this->redirect(['index', 'id_buy' => $model->id_buy]);
            }
        }
        return $this->render('create', [
                    'model' => $model,
                    'buy' => $buy,
                    'company' => $company
        ]);
    }

    /**
     * Updates an existing UtilizedBuy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $buy = $model->idBuy;
        
        if (!Yii::$app->user->identity->is("admin|booker|operator={$buy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $company = $buy->idCompany;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setUtilizedBuy();
            // return $this->redirect(['view', 'id' => $model->id_utilized_buy]);
            return $this->redirect(['index', 'id_buy' => $model->id_buy]);
        }
        return $this->render('update', [
                    'model' => $model,
                    'buy' => $buy,
                    'company' => $company
        ]);
    }

    /**
     * Deletes an existing UtilizedBuy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->is("admin|booker|operator={$model->idBuy->id_company}")) {
            throw new ForbiddenHttpException('Only admin, booker or company operator can see this page.');
        }

        $id_buy = $model->id_buy;
        $model->amount_utilized_buy = 0;
        $model->save();
        $model->setUtilizedBuy();

        $model->delete();

        return $this->redirect(['index', 'id_buy' => $id_buy]);
    }

    /**
     * Finds the UtilizedBuy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UtilizedBuy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UtilizedBuy::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
