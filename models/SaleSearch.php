<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sale;

/**
 * SaleSearch represents the model behind the search form about `app\models\Sale`.
 */
class SaleSearch extends Sale
{
    public $date_sale_max;
    public $date_sale_min;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sale', 'id_company', 'id_product', 'id_partner', 'id_unit', 'is_paid_sale'], 'integer'],
            [['amount', 'value_kurs_sale', 'price_uah_sale', 'price_usd_sale', 'price_per_unit_uah_sale', 'price_per_unit_usd_sale', 'discount_sale'], 'number'],
            [['date_sale','date_sale_max','date_sale_min', 'name_product', 'id_invoice'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sale::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_sale' => $this->id_sale,
            'id_company' => $this->id_company,
            'id_product' => $this->id_product,
            'id_partner' => $this->id_partner,
            'id_unit' => $this->id_unit,
            'amount' => $this->amount,
            'value_kurs_sale' => $this->value_kurs_sale,
            'date_sale' => $this->date_sale,
            'price_uah_sale' => $this->price_uah_sale,
            'price_usd_sale' => $this->price_usd_sale,
            'price_per_unit_uah_sale' => $this->price_per_unit_uah_sale,
            'price_per_unit_usd_sale' => $this->price_per_unit_usd_sale,
            'discount_sale' => $this->discount_sale,
            'is_paid_sale' => $this->is_paid_sale,
        ]);

        $query->andFilterWhere(['like', 'name_product', $this->name_product])
            ->andFilterWhere(['like', 'id_invoice', $this->id_invoice])
            ->andFilterWhere(['<=', 'date_sale', $this->date_sale_max])
            ->andFilterWhere(['>=', 'date_sale', $this->date_sale_min])
        ;

        return $dataProvider;
    }
}
