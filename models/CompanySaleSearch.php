<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
 * CompanySaleSearch represents the model behind the search form about `app\models\Company`.
 */
class CompanySaleSearch extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company'], 'integer'],
            [['name_company', 'kontakt_company', 'note_company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_company' => $this->id_company,
        ]);

        $query->andFilterWhere(['like', 'name_company', $this->name_company])
            ->andFilterWhere(['like', 'kontakt_company', $this->kontakt_company])
            ->andFilterWhere(['like', 'note_company', $this->note_company]);

        return $dataProvider;
    }
}
