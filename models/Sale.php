<?php

namespace app\models;

use Yii;
use yii\db\Query;
/**
 * This is the model class for table "sale".
 *
 * @property integer $id_sale
 * @property integer $id_company
 * @property integer $id_product
 * @property integer $id_partner
 * @property integer $id_unit
 * @property double $amount
 * @property double $value_kurs_sale
 * @property string $date_sale
 * @property string $name_product
 * @property double $price_uah_sale
 * @property double $price_usd_sale
 * @property double $price_per_unit_uah_sale
 * @property double $price_per_unit_usd_sale
 * @property double $discount_sale
 * @property integer $is_paid_sale
 * @property string $id_invoice
 *
 * @property IncomeSale[] $incomeSales
 * @property Company $idCompany
 * @property Product $idProduct
 * @property Partner $idPartner
 * @property Unit $idUnit
 */
class Sale extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sale';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_company', 'id_product', 'id_partner', 'id_unit', 'is_paid_sale'], 'integer'],
            [['id_unit'], 'required'],
            [['amount', 'value_kurs_sale', 'price_uah_sale', 'price_usd_sale', 'price_per_unit_uah_sale', 'price_per_unit_usd_sale', 'discount_sale'], 'number'],
            [['date_sale'], 'safe'],
            [['name_product'], 'string', 'max' => 255],
            [['id_invoice'], 'string', 'max' => 100],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id_product']],
            [['id_partner'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['id_partner' => 'id_partner']],
            [['id_unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['id_unit' => 'id_unit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_sale' => 'ID продажу',
            'id_company' => 'Підприємство',
            'id_product' => 'Товар',
            'id_partner' => 'Партнер',
            'amount' => 'Кількість одиниць товару',
            'id_unit' => 'Одиниці вимірювання',
            'value_kurs_sale' => 'Курс грн/USD',
            'date_sale' => 'Дата продажу',
            'name_product' => 'Назва товару',
            'price_uah_sale' => 'Ціна, грн',
            'price_usd_sale' => 'Ціна, USD',
            'price_per_unit_uah_sale' => 'Ціна за одиницю, ГРН',
            'price_per_unit_usd_sale' => 'Ціна за одиницю, USD',
            'discount_sale' => 'Знижка',
            'is_paid_sale' => 'Оплачено',
            'id_invoice' => 'Накладна №',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeSales() {
        return $this->hasMany(IncomeSale::className(), ['id_sale' => 'id_sale']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany() {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct() {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartner() {
        return $this->hasOne(Partner::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnit() {
        return $this->hasOne(Unit::className(), ['id_unit' => 'id_unit']);
    }

    public function autoPostFill() {
        if (!$this->date_sale) {
            $this->date_sale = date('Y-m-d');
        }

        if(!$this->price_per_unit_uah_sale &&  $this->price_uah_sale){
            $this->price_per_unit_uah_sale = round($this->price_uah_sale/$this->amount,3);
        }

        if( $this->price_per_unit_uah_sale && !$this->price_uah_sale){
            $this->price_uah_sale = round($this->price_per_unit_uah_sale*$this->amount,3);
        }
        
        if(!$this->name_product){
            $saleProduct=Product::findOne($this->id_product);
            $this->name_product=$saleProduct->name_product;
        }
        
        $dateSaleKurs = Kurs::find()->where(['=', 'date_kurs', $this->date_sale])->one();
        $this->value_kurs_sale = $dateSaleKurs->value_kurs;
        $this->price_usd_sale = $dateSaleKurs->getUSDfromUAH($this->price_uah_sale);
        $this->price_per_unit_usd_sale = $dateSaleKurs->getUSDfromUAH($this->price_per_unit_uah_sale);
        
        
    }
    
    public function getAvailableSales($id_company, $id_partner){
        $query = new Query;
        $query->select('sale.*, unit.name_unit,
                        sum(income_sale.amount_usd_income_sale) as incomeSalePaid
                        ')
                ->from('sale')
                ->leftJoin('income_sale', 'sale.id_sale=income_sale.id_sale')
                ->leftJoin('unit', 'unit.id_unit=sale.id_unit')
                ->where([
                    'and',
                    ['=', 'sale.id_company', $id_company],
                    ['=', 'sale.id_partner', $id_partner],
                    ['<>', 'sale.is_paid_sale', 1],
                ])
                ->orderBy('sale.date_sale ASC')
                ->groupBy('sale.id_sale');
        return $query->all();
    }

}
