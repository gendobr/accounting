<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IncomeSale;

/**
 * IncomeSaleSearch represents the model behind the search form about `app\models\IncomeSale`.
 */
class IncomeSaleSearch extends IncomeSale
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_income_sale', 'id_sale', 'id_payment'], 'integer'],
            [['value_kurs_income_sale', 'amount_uah_income_sale', 'amount_usd_income_sale'], 'number'],
            [['date_income_sale'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IncomeSale::find();
        $query->innerJoin('sale', 'sale.id_sale=income_sale.id_sale');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_income_sale' => $this->id_income_sale,
            'id_sale' => $this->id_sale,
            'id_payment' => $this->id_payment,
            'value_kurs_income_sale' => $this->value_kurs_income_sale,
            'date_income_sale' => $this->date_income_sale,
            'amount_uah_income_sale' => $this->amount_uah_income_sale,
            'amount_usd_income_sale' => $this->amount_usd_income_sale,
        ]);

        return $dataProvider;
    }
}
