<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilized_buy".
 *
 * @property integer $id_utilized_buy
 * @property integer $id_buy
 * @property integer $id_utilized_place
 * @property string $date_utilized_buy
 * @property double $amount_utilized_buy
 *
 * @property Buy $idBuy
 * @property UtilizedPlace $idUtilizedPlace
 */
class UtilizedBuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'utilized_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_buy', 'id_utilized_place'], 'integer'],
            [['date_utilized_buy'], 'safe'],
            [['amount_utilized_buy'], 'number'],
            [['id_buy'], 'exist', 'skipOnError' => true, 'targetClass' => Buy::className(), 'targetAttribute' => ['id_buy' => 'id_buy']],
            [['id_utilized_place'], 'exist', 'skipOnError' => true, 'targetClass' => UtilizedPlace::className(), 'targetAttribute' => ['id_utilized_place' => 'id_utilized_place']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_utilized_buy' => 'ID використання',
            'id_buy' => 'Дата покупки',
            'id_utilized_place' => 'Місце використання',
            'date_utilized_buy' => 'Дата використання',
            'amount_utilized_buy' => 'Використана кількість',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBuy()
    {
        return $this->hasOne(Buy::className(), ['id_buy' => 'id_buy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUtilizedPlace()
    {
        return $this->hasOne(UtilizedPlace::className(), ['id_utilized_place' => 'id_utilized_place']);
    }
    
    public function setUtilizedBuy(){
        $buy=$this->idBuy;
        $amount = $buy->getRemainder();
        $buy->is_fully_utilized_buy =($amount<=0)?1:0;
        $buy->save();
    }
}
