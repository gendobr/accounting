<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id_category
 * @property integer $id_company
 * @property string $name_category
 *
 * @property Company $idCompany
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company'], 'integer'],
            [['name_category'], 'string', 'max' => 100],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_category' => 'ID категорії',
            'id_company' => 'Підприємство',
            'name_category' => 'Назва категорії',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany()
    {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id_category' => 'id_category']);
    }
    
    public static function listAll($id_company){
        
        $query = (new \yii\db\Query)
                ->select("c.id_category as k,c.name_category ")
                ->from(['c' => "category"])
                ->where(['c.id_company'=>$id_company])
        ;
        
        $tmp=$query->all();
        $options=[];
        foreach($tmp as $tm){
            $options[$tm['k']] = $tm['name_category'];
        }
        //
        return $options;
    }
}
