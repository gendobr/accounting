<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "income_sale".
 *
 * @property integer $id_income_sale
 * @property integer $id_sale
 * @property integer $id_payment
 * @property double $value_kurs_income_sale
 * @property string $date_income_sale
 * @property double $amount_uah_income_sale
 * @property double $amount_usd_income_sale
 *
 * @property Sale $idSale
 * @property Payment $idPayment
 */
class IncomeSale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'income_sale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sale', 'id_payment'], 'integer'],
            [['value_kurs_income_sale', 'amount_uah_income_sale', 'amount_usd_income_sale'], 'number'],
            [['date_income_sale'], 'safe'],
            [['id_sale'], 'exist', 'skipOnError' => true, 'targetClass' => Sale::className(), 'targetAttribute' => ['id_sale' => 'id_sale']],
            [['id_payment'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['id_payment' => 'id_payment']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_income_sale' => 'ID частини оплати',
            'id_sale' => 'Продаж',
            'id_payment' => 'Платіж',
            'value_kurs_income_sale' => 'Курс',
            'date_income_sale' => 'Дата надходження',
            'amount_uah_income_sale' => 'Величина, грн',
            'amount_usd_income_sale' => 'Величина, USD',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSale()
    {
        return $this->hasOne(Sale::className(), ['id_sale' => 'id_sale']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPayment()
    {
        return $this->hasOne(Payment::className(), ['id_payment' => 'id_payment']);
    }
    
    public function getKurs(){
        $payment = $this->idPayment;
        if($payment){
            $datePaymentKurs = Kurs::find()->where(['=', 'date_kurs', $payment->date_payment])->one();
            return $datePaymentKurs;
        }
        return null;
    }

}
