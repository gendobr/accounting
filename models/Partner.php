<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id_partner
 * @property string $name_partner
 * @property string $kontakt_partner
 * @property string $note_partner
 *
 * @property Buy[] $buys
 * @property Payment[] $payments
 * @property Sale[] $sales
 */
class Partner extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_company'], 'integer'],
            [['name_partner', 'kontakt_partner', 'note_partner'], 'string', 'max' => 255],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_partner' => 'ID партнера',
            'name_partner' => 'Назва або ім\'я',
            'kontakt_partner' => 'Контактна інформація',
            'note_partner' => 'Примітки',
            'id_company' => 'Підприємство'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuys() {
        return $this->hasMany(Buy::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments() {
        return $this->hasMany(Payment::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales() {
        return $this->hasMany(Sale::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany() {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    public static function listAll($id_company) {

        $query = (new \yii\db\Query)
                ->select("c.id_partner as k,c.name_partner ")
                ->from(['c' => "partner"])
                ->where(['c.id_company' => $id_company])
        ;

        $tmp = $query->all();
        $options = [];
        foreach ($tmp as $tm) {
            $options[$tm['k']] = $tm['name_partner'];
        }
        //
        return $options;
    }

}
