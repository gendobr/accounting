<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `app\models\Payment`.
 */
class PaymentSearch extends Payment
{
    public $date_payment_min;
    public $date_payment_max;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_payment', 'id_company', 'id_partner'], 'integer'],
            [['date_payment', 'date_payment_max','date_payment_min','type_payment'], 'safe'],
            [['value_kurs_payment', 'amount_uah_payment', 'amount_usd_payment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_payment' => $this->id_payment,
            'id_company' => $this->id_company,
            'id_partner' => $this->id_partner,
            'date_payment' => $this->date_payment,
            'value_kurs_payment' => $this->value_kurs_payment,
            'amount_uah_payment' => $this->amount_uah_payment,
            'amount_usd_payment' => $this->amount_usd_payment,
        ]);

        $query->andFilterWhere(['like', 'type_payment', $this->type_payment]);

        $query->andFilterWhere(['>=', 'date_payment', $this->date_payment_min]);
        $query->andFilterWhere(['<=', 'date_payment', $this->date_payment_max]);
        
        return $dataProvider;
    }
}
