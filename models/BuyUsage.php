<?php

namespace app\models;

use Yii;
use yii\base\Model;


/**
 * Description of CompanyBalance
 *
 * @author dobro
 */
class BuyUsage extends Model{
    public $id_company; // Підприємство,
    public $id_partner;
    
    public $place;// Об’єкт / підоб’єкт
    public $name_product;// назва_товару
    public $date_utilized_buy_min;
    public $date_utilized_buy_max;
    public $id_invoice;
    
    public function attributeLabels()
    {
        return [
            'id_company' => 'Підприємство',
            'place' => 'Місце використання',
            'name_product' => 'Товар',
            'date_utilized_buy_min'=>'Дата від',
            'date_utilized_buy_max'=>'Дата до',
            'id_invoice'=>'Накладна №',
            'id_partner'=>'Партнер'
        ];
    }
    public function rules() {
        return [
            [[ 'id_company','id_partner'], 'integer'],
            [['place', 'name_product', 'id_invoice', 'date_utilized_buy_min','date_utilized_buy_max'], 'safe'],
        ];
    }
}
