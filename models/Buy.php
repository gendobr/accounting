<?php

namespace app\models;

use Yii;
use yii\db\Query;
/**
 * This is the model class for table "buy".
 *
 * @property integer $id_buy
 * @property integer $id_company
 * @property integer $id_product
 * @property integer $id_partner
 * @property double $value_kurs_buy
 * @property string $name_product
 * @property string $date_buy
 * @property double $amount_buy
 * @property integer $id_unit
 * @property double $price_uah_buy
 * @property double $price_usd_buy
 * @property double $price_per_unit_usd_buy
 * @property double $price_per_unit_uah_buy
 * @property string $id_invoice
 * @property integer $is_fully_paid_buy
 * @property integer $is_fully_utilized_buy
 *
 * @property Company $idCompany
 * @property Product $idProduct
 * @property Partner $idPartner
 * @property Unit $idUnit
 * @property SpendBuy[] $spendBuys
 * @property UtilizedBuy[] $utilizedBuys
 */
class Buy extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'buy';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_company', 'id_product', 'id_partner', 'id_unit', 'is_fully_paid_buy', 'is_fully_utilized_buy'], 'integer'],
            [['value_kurs_buy', 'amount_buy', 'price_uah_buy', 'price_usd_buy', 'price_per_unit_usd_buy', 'price_per_unit_uah_buy'], 'number'],
            [['date_buy'], 'safe'],
            [['name_product'], 'string', 'max' => 255],
            [['id_invoice'], 'string', 'max' => 100],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id_product']],
            [['id_partner'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['id_partner' => 'id_partner']],
            [['id_unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['id_unit' => 'id_unit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_buy' => 'ID покупки',
            'id_company' => 'Підприємство',
            'id_product' => 'Товар',
            'id_partner' => 'Партнер',
            'value_kurs_buy' => 'Курс грн/USD',
            'name_product' => 'Назва товару',
            'date_buy' => 'Дата покупки',
            'amount_buy' => 'Кількість товару',
            'id_unit' => 'Одиниця вимірювання',
            'price_uah_buy' => 'Ціна, грн',
            'price_usd_buy' => 'Ціна, USD',
            'price_per_unit_usd_buy' => 'Ціна за одницю, USD',
            'price_per_unit_uah_buy' => 'Ціна за одницю, грн',
            'id_invoice' => 'Накладна №',
            'is_fully_paid_buy' => 'Повністю оплачено',
            'is_fully_utilized_buy' => 'Повністю використано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany() {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct() {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartner() {
        return $this->hasOne(Partner::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnit() {
        return $this->hasOne(Unit::className(), ['id_unit' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpendBuys() {
        return $this->hasMany(SpendBuy::className(), ['id_buy' => 'id_buy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizedBuys() {
        return $this->hasMany(UtilizedBuy::className(), ['id_buy' => 'id_buy']);
    }

    public function autoPostFill() {
        if (!$this->date_buy) {
            $this->date_buy = date('Y-m-d');
        }

        if (!$this->price_per_unit_uah_buy && $this->price_uah_buy) {
            $this->price_per_unit_uah_buy = round($this->price_uah_buy / $this->amount_buy, 3);
        }

        if ($this->price_per_unit_uah_buy && !$this->price_uah_buy) {
            $this->price_uah_buy = round($this->price_per_unit_uah_buy * $this->amount_buy, 3);
        }

        if (!$this->name_product) {
            $saleProduct = Product::findOne($this->id_product);
            $this->name_product = $saleProduct->name_product;
        }

        $dateSaleKurs = Kurs::find()->where(['=', 'date_kurs', $this->date_buy])->one();
        $this->value_kurs_buy = $dateSaleKurs->value_kurs;
        $this->price_usd_buy = $dateSaleKurs->getUSDfromUAH($this->price_uah_buy);
        $this->price_per_unit_usd_buy = $dateSaleKurs->getUSDfromUAH($this->price_per_unit_uah_buy);
    }

    public function getRemainder() {
        $utilizations = $this->getUtilizedBuys()->all();
        $amount = $this->amount_buy;
        foreach ($utilizations as $utilization) {
            $amount -=$utilization->amount_utilized_buy;
        }
        return $amount;
    }

    public function getAvailableBuys($id_company, $id_partner) {
        $query = new Query;
        $query->select('buy.*, 
                        sum(spend_buy.amount_usd_spend_buy) as spendBuyPaid
                        ')
                ->from('buy')
                ->leftJoin('spend_buy', 'buy.id_buy=spend_buy.id_buy')
                ->where([
                    'and',
                    ['=', 'buy.id_company', $id_company],
                    ['=', 'buy.id_partner', $id_partner],
                    ['<>', 'buy.is_fully_paid_buy', 1],
                ])
                ->orderBy('buy.date_buy ASC')
                ->groupBy('buy.id_buy');
        return $query->all();
    }
    
}
