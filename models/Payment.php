<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id_payment
 * @property integer $id_company
 * @property integer $id_partner
 * @property string $date_payment
 * @property double $value_kurs_payment
 * @property double $amount_uah_payment
 * @property double $amount_usd_payment
 * @property string $type_payment
 *
 * @property IncomeSale[] $incomeSales
 * @property Company $idCompany
 * @property Partner $idPartner
 * @property SpendBuy[] $spendBuys
 */
class Payment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_company', 'id_partner', 'is_used'], 'integer'],
            [['date_payment'], 'safe'],
            [['value_kurs_payment', 'amount_uah_payment', 'amount_usd_payment'], 'number'],
            [['type_payment'], 'string'],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
            [['id_partner'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['id_partner' => 'id_partner']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_payment' => 'ID платежу',
            'id_company' => 'Підприємство',
            'id_partner' => 'Партнер',
            'date_payment' => 'Дата платежу',
            'value_kurs_payment' => 'Значення курсу',
            'amount_uah_payment' => 'Величина, ГРН',
            'amount_usd_payment' => 'Величина, USD',
            'type_payment' => 'Тип платежу',
            'is_used' => 'Використано повністю'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeSales() {
        return $this->hasMany(IncomeSale::className(), ['id_payment' => 'id_payment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany() {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartner() {
        return $this->hasOne(Partner::className(), ['id_partner' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpendBuys() {
        return $this->hasMany(SpendBuy::className(), ['id_payment' => 'id_payment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypeList() {
        return ['income' => 'Вхідний', 'outcome' => 'Вихідний'];
    }

    public function autoFill() {

        if (!$this->date_payment) {
            $this->date_payment = date('Y-m-d');
        }

        $datePaymentKurs = Kurs::find()->where(['=', 'date_kurs', $this->date_payment])->one();
        $this->value_kurs_payment = $datePaymentKurs->value_kurs;
        $this->amount_usd_payment = $datePaymentKurs->getUSDfromUAH($this->amount_uah_payment);
    }
    
    public function getKurs(){
        $datePaymentKurs = Kurs::find()->where(['=', 'date_kurs', $this->date_payment])->one();
        return $datePaymentKurs;
    }

    public function clearPayment() {
        $incomeSales = $this->getIncomeSales()->all();
        foreach ($incomeSales as $incomeSale) {

            $sale = $incomeSale->idSale;
            $sale->is_paid_sale = 0;
            $sale->save();

            $incomeSale->delete();
        }

        $spendBuys = $this->getSpendBuys()->all();
        foreach ($spendBuys as $spendBuy) {
            $buy = $spendBuy->idBuy;
            $buy->is_fully_paid_buy = 0;
            $buy->save();
            $spendBuy->delete();
        }
    }

    public function applyPayment($oldModel = false) {
        $dateSaleKurs = Kurs::find()->where(['=', 'date_kurs', $this->date_payment])->one();
        switch ($this->type_payment) {
            case 'income':
                $saleList = Sale::find()->where([
                            'and',
                            ['=', 'id_company', $this->id_company],
                            ['=', 'id_partner', $this->id_partner],
                            ['<>', 'is_paid_sale', '1']
                        ])->orderBy('date_sale ASC')->all();

                // 

                $balance = $this->amount_usd_payment;
                foreach ($saleList as $sale) {
                    if ($balance > 0.00000001) {
                        $amount_to_pay = $sale->price_usd_sale;
                        $incomeSales = $sale->getIncomeSales()->all();
                        foreach ($incomeSales as $isl) {
                            $amount_to_pay-=$isl->amount_usd_income_sale;
                        }

                        if ($amount_to_pay > 0) {
                            if ($amount_to_pay <= $balance) {
                                $sale->is_paid_sale = 1;
                                $sale->save();
                            }
                            $amount_to_pay = min($amount_to_pay, $balance);
                            $incomeSale = new IncomeSale();
                            // 'id_income_sale' => 'ID частини оплати',
                            $incomeSale->id_sale = $sale->id_sale;
                            $incomeSale->id_payment = $this->id_payment;
                            $incomeSale->value_kurs_income_sale = $dateSaleKurs->value_kurs;
                            $incomeSale->date_income_sale = $this->date_payment;
                            $incomeSale->amount_usd_income_sale = $amount_to_pay;
                            $incomeSale->amount_uah_income_sale = $dateSaleKurs->getUAHfromUSD($incomeSale->amount_usd_income_sale);
                            $incomeSale->save();
                            $balance -= $amount_to_pay;
                        } elseif ($amount_to_pay == 0) {
                            $sale->is_paid_sale = 1;
                            $sale->save();
                        } else {
                            //
                        }
                    } else {
                        break;
                    }
                }
                if ($balance < 0.00000001) {
                    $this->is_used = 1;
                    $this->save();
                } else {
                    $this->is_used = 0;
                    $this->save();
                }

                break;
            case 'outcome':
                $buyList = Buy::find()->where([
                            'and',
                            ['=', 'id_company', $this->id_company],
                            ['=', 'id_partner', $this->id_partner],
                            ['<>', 'is_fully_paid_buy', '1']
                        ])->orderBy('date_buy ASC')->all();

                $balance = $this->amount_usd_payment;
                foreach ($buyList as $buy) {
                    if ($balance > 0.00000001) {
                        $amount_to_pay = $buy->price_usd_buy;
                        $spendBuys = $buy->getSpendBuys()->all();
                        foreach ($spendBuys as $isl) {
                            $amount_to_pay-=$isl->amount_usd_spend_buy;
                        }

                        if ($amount_to_pay > 0) {
                            if ($amount_to_pay <= $balance) {
                                $buy->is_fully_paid_buy = 1;
                                $buy->save();
                            }
                            $amount_to_pay = min($amount_to_pay, $balance);
                            $spendBuy = new SpendBuy();
                            // 'id_spend_buy' => 'ID виплати',
                            $spendBuy->id_buy = $buy->id_buy;
                            $spendBuy->id_payment = $this->id_payment;
                            $spendBuy->amount_usd_spend_buy = $amount_to_pay;
                            $spendBuy->amount_grn_spend_buy = $dateSaleKurs->getUAHfromUSD($spendBuy->amount_usd_spend_buy);
                            $spendBuy->save();
                            $balance -= $amount_to_pay;
                        } elseif ($amount_to_pay == 0) {
                            $buy->is_fully_paid_buy = 1;
                            $buy->save();
                        } else {
                            //
                        }
                    } else {
                        break;
                    }
                }
                if ($balance < 0.00000001) {
                    $this->is_used = 1;
                    $this->save();
                } else {
                    $this->is_used = 0;
                    $this->save();
                }
                break;
        }
    }

    public function getAvailablePayments($id_company, $id_partner) {
        $query = new Query;
        $query->select('payment.*, 
                        sum(spend_buy.amount_usd_spend_buy)/payment.amount_usd_payment as spendBuyPaid,
                        sum(income_sale.amount_usd_income_sale)/payment.amount_usd_payment as incomeSalePaid
                        ')
                ->from('payment')
                ->leftJoin('spend_buy', 'payment.id_payment=spend_buy.id_payment')
                ->leftJoin('income_sale', 'payment.id_payment=income_sale.id_payment')
                ->where([
                    'and',
                    ['=', 'payment.id_company', $id_company],
                    ['=', 'payment.id_partner', $id_partner],
                    ['<>', 'payment.is_used', 1],
                ])
                ->orderBy('payment.date_payment ASC')
                ->groupBy('payment.id_payment');
        return $query->all();
    }

    public function usePayment($id, $id_payment) {

        $payment = Payment::findOne($id_payment);



        switch ($payment->type_payment) {
            // =================================================================
            case 'income':

                $sale = Sale::findOne($id);

                // get unpaid part
                $unpaidPart = $sale->price_usd_sale;
                $incomeSales = $sale->getIncomeSales()->all();
                foreach ($incomeSales as $isl) {
                    $unpaidPart -= $isl->amount_usd_income_sale;
                }

                // get unused part of payment
                $unusedPart = $payment->amount_usd_payment;
                $incomeSales = $payment->getIncomeSales()->all();
                foreach ($incomeSales as $isl) {
                    $unusedPart -= $isl->amount_usd_income_sale;
                }

                // create new incomeSale
                if ($unusedPart < $unpaidPart) {
                    // unused part of payment is less than unpaid part of sale
                    // create new incomeSale
                    $incomeSale = new IncomeSale();
                    $incomeSale->id_sale = $sale->id_sale;
                    $incomeSale->id_payment = $payment->id_payment;
                    $incomeSale->date_income_sale = max($payment->date_payment, $sale->date_sale);
                    $kurs = Kurs::find()->where(['=', 'date_kurs', $incomeSale->date_income_sale])->one();
                    $incomeSale->value_kurs_income_sale = $kurs->value_kurs;
                    $incomeSale->amount_usd_income_sale = $unusedPart;
                    $incomeSale->amount_uah_income_sale = $kurs->getUAHfromUSD($incomeSale->amount_usd_income_sale);
                    $incomeSale->save();

                    // set payment->is_used to true
                    $payment->is_used = true;
                    $payment->save();
                } else {
                    // unused part of payment is greater than unpaid part of sale
                    // create new incomeSale
                    $incomeSale = new IncomeSale();
                    $incomeSale->id_sale = $sale->id_sale;
                    $incomeSale->id_payment = $payment->id_payment;
                    $incomeSale->date_income_sale = max($payment->date_payment, $sale->date_sale);
                    $kurs = Kurs::find()->where(['=', 'date_kurs', $incomeSale->date_income_sale])->one();
                    $incomeSale->value_kurs_income_sale = $kurs->value_kurs;
                    $incomeSale->amount_usd_income_sale = $unpaidPart;
                    $incomeSale->amount_uah_income_sale = $kurs->getUAHfromUSD($incomeSale->amount_usd_income_sale);
                    $incomeSale->save();

                    // set sale->is_paid_sale to true
                    $sale->is_paid_sale = true;
                    $sale->save();

                    if (abs($unusedPart - $unpaidPart) < 0.00001) {
                        $payment->is_used = true;
                        $payment->save();
                    }
                }

                break;
            // =================================================================
            case 'outcome':
                $buy = Buy::findOne($id);

                // get unpaid part
                $unpaidPart = $buy->price_usd_buy;
                $spendBuys = $buy->getSpendBuys()->all();
                foreach ($spendBuys as $isl) {
                    $unpaidPart -= $isl->amount_usd_spend_buy;
                }

                // get unused part of payment
                $unusedPart = $payment->amount_usd_payment;
                $spendBuys = $payment->getSpendBuys()->all();
                foreach ($spendBuys as $isl) {
                    $unusedPart -= $isl->amount_usd_spend_buy;
                }

                // create new SpendBuy
                if ($unusedPart < $unpaidPart) {
                    // unused part of payment is less than unpaid part of buy
                    // create new 
                    $spendBuy = new SpendBuy();
                    $spendBuy->id_payment = $payment->id_payment;
                    $spendBuy->id_buy = $buy->id_buy;

                    $spendBuy->date_spend_buy = max($payment->date_payment, $buy->date_buy);
                    $kurs = Kurs::find()->where(['=', 'date_kurs', $spendBuy->date_spend_buy ])->one();

                    $spendBuy->value_kurs_spend_buy = $kurs->value_kurs;
                    $spendBuy->amount_usd_spend_buy = $unusedPart;
                    $spendBuy->amount_grn_spend_buy = $kurs->getUAHfromUSD($spendBuy->amount_usd_spend_buy);
                    $spendBuy->save();

                    // set payment->is_used to true
                    $payment->is_used = true;
                    $payment->save();
                } else {
                    // unpaid part of buy is less (or equal) than unused part of payment
                    $spendBuy = new SpendBuy();
                    $spendBuy->id_payment = $payment->id_payment;
                    $spendBuy->id_buy = $buy->id_buy;

                    $spendBuy->date_spend_buy = max($payment->date_payment, $buy->date_buy);
                    $kurs = Kurs::find()->where(['=', 'date_kurs', $spendBuy->date_spend_buy ])->one();

                    $spendBuy->value_kurs_spend_buy = $kurs->value_kurs;

                    $spendBuy->amount_usd_spend_buy = $unpaidPart;
                    $spendBuy->amount_grn_spend_buy = $kurs->getUAHfromUSD($spendBuy->amount_usd_spend_buy);

                    $spendBuy->save();

                    $buy->is_fully_paid_buy = true;
                    $buy->save();
                    
                    // set payment->is_used to true
                    if (abs($unusedPart - $unpaidPart) < 0.00001) {
                        $payment->is_used = true;
                        $payment->save();
                    }
                    
                }


                break;
        }
    }

}
