<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id_product
 * @property integer $id_company
 * @property integer $id_category
 * @property integer $id_unit
 * @property double $price_per_unit_usd_product
 * @property string $name_product
 * @property string $note_product
 * @property integer $is_visible_product
 *
 * @property Buy[] $buys
 * @property Company $idCompany
 * @property Category $idCategory
 * @property Unit $idUnit
 * @property Sale[] $sales
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company', 'id_category', 'id_unit', 'is_visible_product'], 'integer'],
            [['price_per_unit_usd_product'], 'number'],
            [['name_product', 'note_product'], 'string', 'max' => 255],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id_category']],
            [['id_unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['id_unit' => 'id_unit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_product' => 'ID товару',
            'id_company' => 'Підприємство',
            'id_category' => 'Категорія',
            'id_unit' => 'Одиниці вимірювання',
            'price_per_unit_usd_product' => 'Ціна за одиницю, USD',
            'name_product' => 'Назва товару',
            'note_product' => 'Примітки',
            'is_visible_product' => 'Видимість',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuys()
    {
        return $this->hasMany(Buy::className(), ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany()
    {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id_category' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnit()
    {
        return $this->hasOne(Unit::className(), ['id_unit' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sale::className(), ['id_product' => 'id_product']);
    }
    
    public static function listAll($id_company){
        $query = (new \yii\db\Query)
                ->select("p.id_product as k, p.name_product ")
                ->from(['p' => "product"])
                ->where(['p.id_company'=>$id_company])
        ;
        
        $tmp=$query->all();
        $options=[];
        foreach($tmp as $tm){
            $options[$tm['k']] = $tm['name_product'];
        }
        //
        return $options;
    }
}
