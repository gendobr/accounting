<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id_unit
 * @property integer $id_company
 * @property string $name_unit
 *
 * @property Buy[] $buys
 * @property Product[] $products
 * @property Sale[] $sales
 * @property Company $idCompany
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id_unit', 'id_company'], 'required'],
            [['id_unit', 'id_company'], 'integer'],
            [['name_unit'], 'string', 'max' => 32],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_unit' => 'ID одиниці вимірювання',
            'id_company' => 'Підприємство',
            'name_unit' => 'Назва одиниці вимірювання',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuys()
    {
        return $this->hasMany(Buy::className(), ['id_unit' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id_unit' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sale::className(), ['id_unit' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany()
    {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }
    
    
    public static function listAll($id_company){
        $query = (new \yii\db\Query)
                ->select("u.id_unit as k,u.name_unit ")
                ->from(['u' => "unit"])
                ->where(['u.id_company'=>$id_company])
        ;
        
        $tmp=$query->all();
        $options=[];
        foreach($tmp as $tm){
            $options[$tm['k']] = $tm['name_unit'];
        }
        //
        return $options;
    }
}
