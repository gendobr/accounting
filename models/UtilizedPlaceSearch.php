<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UtilizedPlace;

/**
 * UtilizedPlaceSearch represents the model behind the search form about `app\models\UtilizedPlace`.
 */
class UtilizedPlaceSearch extends UtilizedPlace
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_utilized_place', 'id_company', 'id_utilized_place_parent', 'is_visible_utilized_place'], 'integer'],
            [['name_utilized_place', 'address_utilized_place', 'notes_utilized_place'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UtilizedPlace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_utilized_place' => $this->id_utilized_place,
            'id_company' => $this->id_company,
            'id_utilized_place_parent' => $this->id_utilized_place_parent,
            'is_visible_utilized_place' => $this->is_visible_utilized_place,
        ]);

        $query->andFilterWhere(['like', 'name_utilized_place', $this->name_utilized_place])
            ->andFilterWhere(['like', 'address_utilized_place', $this->address_utilized_place])
            ->andFilterWhere(['like', 'notes_utilized_place', $this->notes_utilized_place]);

        return $dataProvider;
    }
}
