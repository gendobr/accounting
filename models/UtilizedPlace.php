<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilized_place".
 *
 * @property integer $id_utilized_place
 * @property integer $id_company
 * @property string $name_utilized_place
 * @property string $address_utilized_place
 * @property string $notes_utilized_place
 * @property integer $id_utilized_place_parent
 * @property integer $is_visible_utilized_place
 *
 * @property UtilizedBuy[] $utilizedBuys
 * @property Company $idCompany
 */
class UtilizedPlace extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'utilized_place';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_company', 'id_utilized_place_parent', 'is_visible_utilized_place'], 'integer'],
            [['name_utilized_place', 'address_utilized_place', 'notes_utilized_place'], 'string', 'max' => 255],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_utilized_place' => 'ID місця використання',
            'id_company' => 'Підприємство',
            'name_utilized_place' => 'Назва місця використання',
            'address_utilized_place' => 'Адреса місця використання',
            'notes_utilized_place' => 'Примітки',
            'id_utilized_place_parent' => 'Є частиною',
            'is_visible_utilized_place' => 'Видиме місце використання',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizedBuys() {
        return $this->hasMany(UtilizedBuy::className(), ['id_utilized_place' => 'id_utilized_place']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompany() {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    public static function listAll($id_company, $isNewRecord = false) {

        $filter=['and'];
        $filter['c.id_company']= $id_company;
        if($isNewRecord){
            $filter['c.is_visible_utilized_place']=1;
        }
        $query = (new \yii\db\Query)
                ->select("c.id_utilized_place as k, c.name_utilized_place, c.address_utilized_place, c.id_utilized_place_parent ")
                ->from(['c' => "utilized_place"])
                ->where([])
                ->orderBy("c.id_utilized_place_parent ASC")
        ;

        $tmp = $query->all();
        $options = [];
        foreach ($tmp as $tm) {
            // $options[$tm['k']] = trim("{$tm['name_utilized_place']} {$tm['address_utilized_place']}");
            $tm['path'] = (isset($options[$tm['id_utilized_place_parent']]) ? $options[$tm['id_utilized_place_parent']]['path'] : '') . '/' . $tm['id_utilized_place_parent'];
            
            $tm['deep'] = 1+(isset($options[$tm['id_utilized_place_parent']]) ? $options[$tm['id_utilized_place_parent']]['deep'] : 0);
            $options[$tm['k']] = $tm;
        }
        //
        uasort($options, function($a, $b) {
            return strcmp($a['path'], $b['path']);
        });
        $tor = array_map(function($tm){
            return trim(str_repeat ( " + " , $tm['deep'])."{$tm['name_utilized_place']} {$tm['address_utilized_place']}");
        }, $options);


        return $tor;
    }

}
