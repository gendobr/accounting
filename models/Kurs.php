<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kurs".
 *
 * @property integer $id_kurs
 * @property string $date_kurs
 * @property double $value_kurs
 */
class Kurs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_kurs'], 'safe'],
            [['value_kurs'], 'number'],
            [['date_kurs', 'value_kurs'], 'unique', 'targetAttribute' => ['date_kurs', 'value_kurs'], 'message' => 'The combination of Дата курсу and Значення курсу has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kurs' => 'ID курсу',
            'date_kurs' => 'Дата курсу',
            'value_kurs' => 'Значення курсу', // UAH/USD
        ];
    }
    
    public function getUSDfromUAH($amountUAH){
        return $amountUAH / $this->value_kurs;
    }
    public function getUAHfromUSD($amountUSD){
        return $amountUSD * $this->value_kurs;
    }
}
