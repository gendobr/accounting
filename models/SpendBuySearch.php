<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SpendBuy;

/**
 * SpendBuySearch represents the model behind the search form about `app\models\SpendBuy`.
 */
class SpendBuySearch extends SpendBuy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_spend_buy', 'id_payment', 'id_buy'], 'integer'],
            [['amount_grn_spend_buy', 'amount_usd_spend_buy'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SpendBuy::find();
        $query->innerJoin('buy', 'buy.id_buy=spend_buy.id_buy');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_spend_buy' => $this->id_spend_buy,
            'id_payment' => $this->id_payment,
            'id_buy' => $this->id_buy,
            'amount_grn_spend_buy' => $this->amount_grn_spend_buy,
            'amount_usd_spend_buy' => $this->amount_usd_spend_buy,
        ]);

        return $dataProvider;
    }
}
