<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * This is the model class for table "user".
 *
 * @property integer $id_user
 * @property string $lastname_user
 * @property string $firstname_user
 * @property string $surname_user
 * @property string $login_user
 * @property string $password_user
 * @property string $email_user
 * @property string $note_user
 * @property string $role_user
 * @property integer $id_company
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    private $roles = [];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['lastname_user', 'firstname_user', 'surname_user', 'login_user', 'password_user', 'email_user', 'access_token', 'auth_key'], 'string', 'max' => 100],
            [['note_user'], 'string', 'max' => 255],
            [ ['id_role'], 'each', 'rule' => ['integer']
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'id_role' => 'roleUser',
                ],
            ],
        ];
    }

    public function getRoleUser() {
        return $this->hasMany(Role::className(), ['id_role' => 'id_role'])
                        ->viaTable('role_user', ['id_user' => 'id_user']);
    }

    public static function listAll() {
        $query = (new \yii\db\Query)
                ->select("r.id_role as k, `com`.`name_company`, r.name_role ")
                ->from(['r' => "role"])
                ->leftJoin(['com' => 'company'], 'com.id_company = r.id_company')
        ;

        $tmp = $query->all();
        $options = [];
        foreach ($tmp as $tm) {
            $options[$tm['k']] = "{$tm['name_role']} {$tm['name_company']} ";
        }
        //
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_user' => 'ID користувача',
            'lastname_user' => 'Прізвище',
            'firstname_user' => 'Ім\'я',
            'surname_user' => 'По батькові',
            'login_user' => 'Login користувача',
            'password_user' => 'Password User',
            'email_user' => 'Електропошта',
            'note_user' => 'Примітки',
            'access_token' => 'access_token',
            'auth_key' => 'auth_key',
            'id_role' => 'Роль користувача'
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        $user = User::find()->where(['access_token' => $token])->one();
        return $user;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $user = User::find()->where(['login_user' => $username])->one();
        // var_dump($user);
        return $user;
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id_user;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        $encodedPassword = crypt($password, Yii::$app->params['salt'] . $this->id_user);
        header("x-dbg-001: {$password}+{$this->id_user}+{$encodedPassword}+{$this->password_user}");
        return $encodedPassword == $this->password_user;
    }

    public function setPassword($newPassword) {
        $this->password_user = crypt($newPassword, Yii::$app->params['salt'] . $this->id_user);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles() {
        $subqueryRoleId = (new \yii\db\Query)
                ->select('ru.id_role')
                ->from(['ru' => "role_user"])
                ->andFilterWhere(['=', 'ru.id_user', $this->id_user])
        ;
        // var_dump($subqueryRoleId->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);exit();
        // var_dump(Role::find()->where(['and', ['in', 'id_role', $subqueryRoleId]])->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);exit();
        return Role::find()->where(['and', ['in', 'id_role', $subqueryRoleId]])->all();
    }

    /**
     * @return true if user has role listed in $code_role
     * if (!Yii::$app->user->identity->is("admin")) {...}
     * if (!Yii::$app->user->identity->is("admin|booker")) {...}
     * if (!Yii::$app->user->identity->is("admin|booker|operator=$id_company")) {...}
     */
    public function is($code_role) {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (count($this->roles) == 0) {
            $this->roles = $this->getRoles();
        }
        $roleList = array_map(function($a) {
            $tmp = explode('=', $a);
            $tmp[1] = isset($tmp[1]) ? $tmp[1] : '';
            return ['code_role' => $tmp[0], 'id_company' => $tmp[1]];
        }, explode("|", $code_role));

        foreach ($roleList as $rl) {
            foreach ($this->roles as $r) {
                if ($r->code_role == $rl['code_role'] || $rl['code_role'] == '*') {
                    if ($r->id_company) {
                        if ($rl['id_company'] == $r->id_company || $rl['id_company'] == '*') {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getCompanyIds() {
        $query = new Query;
        $query->select('r.id_company')
                ->from(['ru' => 'role_user'])
                ->leftJoin(['r' => 'role'], 'ru.id_role=r.id_role')
                ->where([
                    'and',
                    ['=', 'ru.id_user', $this->id_user],
                    ['>', 'r.id_company', 0],
        ]);
        $company_ids = 
            array_filter(
                array_map(
                    function($c) { return $c['id_company']; }, 
                    $query->all()
                ),
                function($el){return $el>0;}
            );
        return $company_ids;
    }

}
