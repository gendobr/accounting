<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UtilizedBuy;

/**
 * UtilizedBuySearch represents the model behind the search form about `app\models\UtilizedBuy`.
 */
class UtilizedBuySearch extends UtilizedBuy {

    public $id_company;
    public $places;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_utilized_buy', 'id_buy', 'id_company'], 'integer'],
            [['date_utilized_buy', 'id_utilized_place', 'places'], 'safe'],
            [['amount_utilized_buy'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {


        // get all places for the given company
        $tmp = (new \yii\db\Query)
                ->select(['upl.id_utilized_place', 'upl.id_utilized_place_parent', 'upl.name_utilized_place', 'upl.address_utilized_place', 'upl.notes_utilized_place'])
                ->from(['upl' => "utilized_place"])
                ->andFilterWhere(['id_company' => $this->id_company])
                ->orderBy('upl.id_utilized_place_parent ASC')
                ->all();
        $this->places = [];
        foreach ($tmp as $tm) {
            $this->places[$tm['id_utilized_place']] = $tm;
        }

        // collect words from places
        $cnt = array_keys($this->places);
        foreach ($cnt as $key) {
            $this->places[$key]['words'] = "{$this->places[$key]['name_utilized_place']} {$this->places[$key]['address_utilized_place']} {$this->places[$key]['notes_utilized_place']} ";
            if (isset($this->places[$this->places[$key]['id_utilized_place_parent']])) {
                $this->places[$key]['words'].=" {$this->places[$this->places[$key]['id_utilized_place_parent']]['words']}";
            }
        }
        // var_dump($this->places);
        $query = UtilizedBuy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_utilized_buy' => $this->id_utilized_buy,
            'id_buy' => $this->id_buy,
            // 'id_utilized_place' => $this->id_utilized_place,
            'date_utilized_buy' => $this->date_utilized_buy,
            'amount_utilized_buy' => $this->amount_utilized_buy,
        ]);

        if ($this->id_utilized_place) {

            // do search
            $words = array_filter(preg_split("/[- :;!.,]+/", $this->id_utilized_place), function($w) {
                return strlen($w) > 0;
            });
            $ids = array_keys($this->places);
            foreach ($words as $word) {
                for ($i = count($ids) - 1; $i >= 0; $i--) {
                    if (!strstr($this->places[$ids[$i]]['words'], $word)) {
                        unset($ids[$i]);
                    }
                }
                $ids = array_values($ids);
            }
            $ids[] = 0;

            // var_dump($ids);

            $query->andFilterWhere(['in', 'id_utilized_place', $ids]);

            // exit();
        }
        return $dataProvider;
    }

}
