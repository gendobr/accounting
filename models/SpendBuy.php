<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spend_buy".
 *
 * @property integer $id_spend_buy
 * @property integer $id_payment
 * @property integer $id_buy
 * @property double $amount_grn_spend_buy
 * @property double $amount_usd_spend_buy
 *
 * @property Buy $idBuy
 * @property Payment $idPayment
 */
class SpendBuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spend_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_payment', 'id_buy'], 'integer'],
            [['amount_grn_spend_buy', 'amount_usd_spend_buy','value_kurs_spend_buy'], 'number'],
            [['id_buy'], 'exist', 'skipOnError' => true, 'targetClass' => Buy::className(), 'targetAttribute' => ['id_buy' => 'id_buy']],
            [['id_payment'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['id_payment' => 'id_payment']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_spend_buy' => 'ID виплати',
            'id_payment' => 'ID платежу',
            'id_buy' => 'ID покупки',
            'amount_grn_spend_buy' => 'Величина виплати, ГРН',
            'amount_usd_spend_buy' => 'Величина виплати, USD',
            'value_kurs_spend_buy' => 'Курс ГРН/USD'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBuy()
    {
        return $this->hasOne(Buy::className(), ['id_buy' => 'id_buy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPayment()
    {
        return $this->hasOne(Payment::className(), ['id_payment' => 'id_payment']);
    }
}
