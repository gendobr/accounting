<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Buy;

/**
 * BuySearch represents the model behind the search form about `app\models\Buy`.
 */
class BuySearch extends Buy {

    public $date_buy_min;
    public $date_buy_max;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_buy', 'id_company', 'id_product', 'id_partner', 'id_unit', 'is_fully_paid_buy', 'is_fully_utilized_buy'], 'integer'],
            [['value_kurs_buy', 'amount_buy', 'price_uah_buy', 'price_usd_buy', 'price_per_unit_usd_buy', 'price_per_unit_uah_buy'], 'number'],
            [['name_product', 'date_buy','date_buy_min','date_buy_max', 'id_invoice'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Buy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_buy' => $this->id_buy,
            'id_company' => $this->id_company,
            'id_product' => $this->id_product,
            'id_partner' => $this->id_partner,
            'value_kurs_buy' => $this->value_kurs_buy,
            'date_buy' => $this->date_buy,
            'amount_buy' => $this->amount_buy,
            'id_unit' => $this->id_unit,
            'price_uah_buy' => $this->price_uah_buy,
            'price_usd_buy' => $this->price_usd_buy,
            'price_per_unit_usd_buy' => $this->price_per_unit_usd_buy,
            'price_per_unit_uah_buy' => $this->price_per_unit_uah_buy,
            'is_fully_paid_buy' => $this->is_fully_paid_buy,
            'is_fully_utilized_buy' => $this->is_fully_utilized_buy,
        ]);

        $query->andFilterWhere(['like', 'name_product', $this->name_product])
              ->andFilterWhere(['like', 'id_invoice', $this->id_invoice])
              ->andFilterWhere(['>=', 'date_buy', $this->date_buy_min])
              ->andFilterWhere(['<=', 'date_buy', $this->date_buy_max])
        ;


        return $dataProvider;
    }

}
