<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Description of Report
 *
 * @author dobro
 */
class Report {

    //put your code here
    // 2 Звіт: Перелік покупок для заданого контрагента
    // Товар, підприємство, дата, кількість, ціна_ГРН, ціна_USD, оплачено %
    public function saleReport($params) {

        $model = new SaleSearch();

        $query = new Query;
        $query->select('sale.*, sum(income_sale.amount_usd_income_sale)/sale.price_usd_sale as partPaid')
                ->from('sale')
                ->leftJoin('income_sale', 'sale.id_sale=income_sale.id_sale')
                ->groupBy('sale.id_sale');

        $model->load($params);

        if ($model->validate()) {
            // grid filtering conditions
            $query->andFilterWhere([
                'sale.id_sale' => $model->id_sale,
                'sale.id_company' => $model->id_company,
                'sale.id_product' => $model->id_product,
                'sale.id_partner' => $model->id_partner,
                'sale.id_unit' => $model->id_unit,
                'sale.amount' => $model->amount,
                'sale.value_kurs_sale' => $model->value_kurs_sale,
                'sale.date_sale' => $model->date_sale,
                'sale.price_uah_sale' => $model->price_uah_sale,
                'sale.price_usd_sale' => $model->price_usd_sale,
                'sale.price_per_unit_uah_sale' => $model->price_per_unit_uah_sale,
                'sale.price_per_unit_usd_sale' => $model->price_per_unit_usd_sale,
                'sale.discount_sale' => $model->discount_sale,
                'sale.is_paid_sale' => $model->is_paid_sale,
            ]);

            $query->andFilterWhere(['like', 'sale.name_product', $model->name_product])
                    ->andFilterWhere(['like', 'sale.id_invoice', $model->id_invoice])
                    ->andFilterWhere(['<=', 'sale.date_sale', $model->date_sale_max])
                    ->andFilterWhere(['>=', 'sale.date_sale', $model->date_sale_min])
            ;
        }

        $dataProvider = new ActiveDataProvider([ 'query' => $query,]);
        $dataProvider->setSort([
            'attributes' => [
                'id_sale',
                'id_company',
                'id_product',
                'name_product',
                'id_partner',
                'id_unit',
                'amount',
                'value_kurs_sale',
                'date_sale',
                'price_uah_sale',
                'price_usd_sale',
                'price_per_unit_uah_sale',
                'price_per_unit_usd_sale',
                'discount_sale',
                'is_paid_sale',
                'partPaid',
                'id_invoice'
            ]
        ]);


        return [
            'model' => $model,
            'query' => $query,
            'dataProvider' => $dataProvider
        ];
    }

    public function buyReport($params) {
        $model = new BuySearch();
        $query = new Query;
        $query->select('buy.*, sum(spend_buy.amount_usd_spend_buy)/buy.price_usd_buy as partPaid')
                ->from('buy')
                ->leftJoin('spend_buy', 'buy.id_buy=spend_buy.id_buy')
                ->groupBy('buy.id_buy');

        $model->load($params);

        if ($model->validate()) {
            // grid filtering conditions
            $query->andFilterWhere([
                'buy.id_buy' => $model->id_buy,
                'buy.id_company' => $model->id_company,
                'buy.id_product' => $model->id_product,
                'buy.id_partner' => $model->id_partner,
                'buy.value_kurs_buy' => $model->value_kurs_buy,
                'buy.date_buy' => $model->date_buy,
                'buy.amount_buy' => $model->amount_buy,
                'buy.id_unit' => $model->id_unit,
                'buy.price_uah_buy' => $model->price_uah_buy,
                'buy.price_usd_buy' => $model->price_usd_buy,
                'buy.price_per_unit_usd_buy' => $model->price_per_unit_usd_buy,
                'buy.price_per_unit_uah_buy' => $model->price_per_unit_uah_buy,
                'buy.is_fully_paid_buy' => $model->is_fully_paid_buy,
                'buy.is_fully_utilized_buy' => $model->is_fully_utilized_buy,
            ]);

            $query->andFilterWhere(['like', 'buy.name_product', $model->name_product])
                    ->andFilterWhere(['like', 'buy.id_invoice', $model->id_invoice])
                    ->andFilterWhere(['>=', 'buy.date_buy', $model->date_buy_min])
                    ->andFilterWhere(['<=', 'buy.date_buy', $model->date_buy_max])
            ;
        }

        $dataProvider = new ActiveDataProvider([ 'query' => $query,]);
        $dataProvider->setSort([
            'attributes' => [
                'id_buy',
                'id_company',
                'id_product',
                'id_partner',
                'value_kurs_buy',
                'name_product',
                'date_buy',
                'amount_buy',
                'id_unit',
                'price_uah_buy',
                'price_usd_buy',
                'price_per_unit_usd_buy',
                'price_per_unit_uah_buy',
                'id_invoice',
                'is_fully_paid_buy',
                'is_fully_utilized_buy',
                'partPaid',
            ]
        ]);

        return [
            'model' => $model,
            'query' => $query,
            'dataProvider' => $dataProvider
        ];
    }

    public function paymentReport($params,$company_ids=false) {
        $model = new PaymentSearch();
        $query = new Query;
        $query->select('payment.*, 
                        sum(spend_buy.amount_usd_spend_buy)/payment.amount_usd_payment as spendBuyPaid,
                        sum(income_sale.amount_usd_income_sale)/payment.amount_usd_payment as incomeSalePaid
                        ')
                ->from('payment')
                ->leftJoin('spend_buy', 'payment.id_payment=spend_buy.id_payment')
                ->leftJoin('income_sale', 'payment.id_payment=income_sale.id_payment')
                ->groupBy('payment.id_payment');


        $model->load($params);

        if ($model->validate()) {
            // grid filtering conditions
            $query->andFilterWhere([
                'id_payment' => $model->id_payment,
                'id_company' => $model->id_company,
                'id_partner' => $model->id_partner,
                'date_payment' => $model->date_payment,
                'value_kurs_payment' => $model->value_kurs_payment,
                'amount_uah_payment' => $model->amount_uah_payment,
                'amount_usd_payment' => $model->amount_usd_payment,
            ]);

            $query->andFilterWhere(['like', 'type_payment', $model->type_payment]);
            $query->andFilterWhere(['>=', 'date_payment', $model->date_payment_min]);
            $query->andFilterWhere(['<=', 'date_payment', $model->date_payment_max]);
        }
        
        if($company_ids!==false){
            $query->andFilterWhere(['in', 'id_company', $company_ids]);
        }

        $dataProvider = new ActiveDataProvider([ 'query' => $query,]);
        $dataProvider->setSort([
            'attributes' => [
                'spendBuyPaid',
                'incomeSalePaid',
                'id_payment',
                'id_company',
                'id_partner',
                'date_payment',
                'value_kurs_payment',
                'amount_uah_payment',
                'amount_usd_payment',
                'type_payment',
                'is_used',
            ]
        ]);
        // enum('income','outcome')

        return [
            'model' => $model,
            'query' => $query,
            'dataProvider' => $dataProvider
        ];
    }

    public function getCompanyOptions($company_ids=false) {
        if($company_ids===false){
            return ArrayHelper::map(Company::find()->all(), 'id_company', 'name_company');
        }else{
            return ArrayHelper::map(
                    Company::find()
                       ->andFilterWhere(['in', 'id_company', $company_ids])
                       ->all(),
                    'id_company',
                    'name_company'
            );
        }
        
    }

    public function getPartnerOptions($company_ids=false) {
        if ($company_ids!==false) {
            $query = new Query;
            $query->select('partner.*')
                    ->from('partner')
                    ->where(['and',['in', 'id_company', $company_ids]])
            ;
            return ArrayHelper::map($query->all(), 'id_partner', 'name_partner');
        } else {
            $query = new Query;
            $query->select([
                        "partner.id_partner",
                        "CONCAT(`partner`.`name_partner`,
                        '@',
                        `company`.`name_company`) as name_partner"
                    ])
                    ->from('partner')
                    ->innerJoin('company', 'company.id_company=partner.id_company')
            ;
            return ArrayHelper::map($query->all(), 'id_partner', 'name_partner');
        }
    }

    public function getUnitOptions($company_ids=false) {
        if ($company_ids!==false) {
            $query = new Query;
            $query->select('unit.*')
                    ->from('unit')
                    ->where(['and',['in', 'id_company', $company_ids]])
            ;
            return ArrayHelper::map($query->all(), 'id_unit', 'name_unit');
        } else {
            $query = new Query;
            $query->select([
                        "unit.id_unit",
                        "unit.name_unit"
                    ])
                    ->from('unit')
            ;
            return ArrayHelper::map($query->all(), 'id_unit', 'name_unit');
        }
    }

    public function balanceReport($params,$company_ids=false) {
        $model = new PartnerSearch();

        $query = new Query;
        $query->select('company.id_company, company.name_company, 
                        partner.id_partner, partner.name_partner,
                        sum(paymentIn.amount_usd_payment) as paymentInAmount,
                        sum(sale.price_usd_sale) as saleAmount,

                        sum(paymentIn.amount_usd_payment) - sum(sale.price_usd_sale) as saleBalance,


                        sum(paymentOut.amount_usd_payment) as paymentOutAmount,
                        sum(buy.price_usd_buy) as buyAmount,
                      - sum(paymentOut.amount_usd_payment) + sum(buy.price_usd_buy) as buyBalance
                        ')
                ->from('company')
                ->innerJoin('partner', 'partner.id_company=company.id_company')
                ->leftJoin('payment as paymentIn', " ( paymentIn.id_partner=partner.id_partner AND  paymentIn.type_payment='income' )")
                ->leftJoin('payment as paymentOut', " ( paymentOut.id_partner=partner.id_partner AND  paymentOut.type_payment='outcome' )")
                ->leftJoin('sale', " ( sale.id_partner=partner.id_partner AND  sale.id_company=company.id_company )")
                ->leftJoin('buy', " ( buy.id_partner=partner.id_partner AND  buy.id_company=company.id_company )")
                ->groupBy('company.id_company, partner.id_partner');

        $model->load($params);
        if ($model->validate()) {
            $query->andFilterWhere([
                'partner.id_partner' => $model->id_partner,
                'partner.id_company' => $model->id_company,
            ]);
            $query->andFilterWhere(['like', 'partner.name_partner', $model->name_partner]);
        }
        if($company_ids!==false){
            $query->andFilterWhere(['in', 'company.id_company', $company_ids]);
        }
        $dataProvider = new ActiveDataProvider([ 'query' => $query,]);
        $dataProvider->setSort([
            'attributes' => [
                'paymentInAmount',
                'saleAmount',
                'saleBalance',
                'paymentOutAmount',
                'buyAmount',
                'buyBalance',
                'id_company',
                'name_company',
                'id_partner',
                'name_partner',
            ]
        ]);

        return [
            'model' => $model,
            'query' => $query,
            'dataProvider' => $dataProvider
        ];
    }

    public function usageReport($params) {
        $model = new BuyUsage();


        // get all places
        $tmp = (new \yii\db\Query)
                ->select([
                    'upl.id_utilized_place',
                    'upl.id_utilized_place_parent',
                    'upl.name_utilized_place',
                    'upl.address_utilized_place',
                    'upl.notes_utilized_place',
                    'company.id_company',
                    'company.name_company'
                ])
                ->from(['upl' => "utilized_place"])
                ->innerJoin('company', 'upl.id_company=company.id_company')
                ->orderBy('upl.id_company ASC, upl.id_utilized_place_parent ASC')
                ->all();
        $places = [];
        foreach ($tmp as $tm) {
            $places[$tm['id_utilized_place']] = $tm;
            $places[$tm['id_utilized_place']]['words']='';
        }

        // collect words from places
        $cnt = array_keys($places);
        foreach ($cnt as $key) {
            $places[$key]['words'] = "{$places[$key]['name_utilized_place']} {$places[$key]['address_utilized_place']} {$places[$key]['notes_utilized_place']} ";
            if (isset($places[$places[$key]['id_utilized_place_parent']])) {
                $places[$key]['words'].=" ".$places[$places[$key]['id_utilized_place_parent']]['words'];
            }
        }

        //SELECT buy.`id_invoice`, 
        //       buy.id_unit,
        //       buy.`id_product`,
        //       buy.`name_product`,
        //       company.`name_company`,
        //       utilized_buy.*
        //       
        //FROM `utilized_buy` 
        //INNER JOIN buy ON buy.id_buy=utilized_buy.id_buy
        //INNER JOIN `company` ON buy.`id_company`=company.`id_company`
        $query = new Query;
        $query->select("buy.`id_invoice`,buy.`id_partner`, buy.id_unit, buy.`id_product`, buy.`name_product`, company.`name_company`, company.`id_company`, utilized_buy.* ")
                ->from('utilized_buy')
                ->innerJoin("buy", 'buy.id_buy=utilized_buy.id_buy')
                ->innerJoin("company", 'buy.`id_company`=company.`id_company`');

        $model->load($params);
        if ($model->validate()) {

            // 'place' => 'Місце використання',

            $query->andFilterWhere([
                'buy.id_partner' => $model->id_partner,
                'buy.id_company' => $model->id_company,
            ]);
            $query->andFilterWhere(['like', 'buy.id_invoice', $model->id_invoice]);
            $query->andFilterWhere(['like', 'buy.name_product', $model->name_product]);

            $query->andFilterWhere(['>=', 'utilized_buy.date_utilized_buy', $model->date_utilized_buy_min]);
            $query->andFilterWhere(['<=', 'utilized_buy.date_utilized_buy', $model->date_utilized_buy_max]);

            $query->andFilterWhere(['like', 'buy.name_product', $model->name_product]);


            if ($model->place) {

                // do search
                $words = array_filter(preg_split("/[- :;!.,]+/", $model->place), function($w) {
                    return strlen($w) > 0;
                });
                $ids = array_keys($places);
                foreach ($words as $word) {
                    for ($i = count($ids) - 1; $i >= 0; $i--) {
                        if (!strstr($places[$ids[$i]]['words'], $word)) {
                            unset($ids[$i]);
                        }
                    }
                    $ids = array_values($ids);
                }
                $ids[] = 0;

                // var_dump($ids);

                $query->andFilterWhere(['in', 'utilized_buy.id_utilized_place', $ids]);

                // exit();
            }
        }

        $dataProvider = new ActiveDataProvider([ 'query' => $query,]);
        $dataProvider->setSort([
            'attributes' => [
                'id_invoice',
                'id_unit',
                'id_product',
                'id_partner',
                'name_product',
                'name_company',
                'id_company',
                'date_utilized_buy',
            ]
        ]);
        return [
            'model' => $model,
            'query' => $query,
            'dataProvider' => $dataProvider,
            'places'=>$places
        ];
    }

}
