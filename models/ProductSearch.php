<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_product', 'id_company', 'id_category', 'id_unit', 'is_visible_product'], 'integer'],
            [['price_per_unit_usd_product'], 'number'],
            [['name_product', 'note_product'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_product' => $this->id_product,
            'id_company' => $this->id_company,
            'id_category' => $this->id_category,
            'id_unit' => $this->id_unit,
            'price_per_unit_usd_product' => $this->price_per_unit_usd_product,
            'is_visible_product' => $this->is_visible_product,
        ]);

        $query->andFilterWhere(['like', 'name_product', $this->name_product])
                ->andFilterWhere(['like', 'note_product', $this->note_product]);

        return $dataProvider;
    }

}
