<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CryptController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * $ php yii crypt/index <string samplePassword> <int id_user>
     */
    public function actionIndex($password = '', $id_user='')
    {
        echo "salt=".\Yii::$app->params['salt']." password=$password id_user=$id_user\n";
        echo crypt($password, \Yii::$app->params['salt'].$id_user) . "\n";
    }
}
